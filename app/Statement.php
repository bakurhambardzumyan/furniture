<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statement extends Model
{
    protected $fillable = [
        'statement_type', 
        'statement_part', 
        'en_title',
        'ru_title',
        'hy_title',
        'en_street',
        'ru_street',
        'hy_street',
        'en_text',
        'ru_text',
        'hy_text',
        'price',
        'currency',
        'rooms',
        'area',
        'floor',
        'building_type',
        'top_news',
        'region'
    ];


    public function images()
    {
        return $this->hasMany('App\Image');
    }
}
