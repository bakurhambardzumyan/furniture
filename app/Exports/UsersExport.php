<?php

namespace App\Exports;

use App\User;
use App\Statement;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;

class UsersExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    /**
    * @return \Illuminate\Support\Collection
    */

	protected $statements;

	public function  __construct($statements) {
        $this->statements = $statements; 
 	}

    public function collection()
    {
        return $this->statements;
    }


    public function headings(): array
    {
        return [
            'Համար',
            'Հայտարարության տեսակը',
            'Հայտարարության բաժինը',
            'Տարածաշրջան',
            'Գինը',
            'Տարադրամ',
            'Սենյակների քանակ',
            'Մակերեսը',
            'Հարկը',
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function(AfterSheet $event) {
                $cellRange = 'A1:W1'; // All headers
                $event->sheet->getDelegate()->getStyle($cellRange)->getFont()->setSize(12);
            },
        ];
    }
}
