<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = [
    	'statement_id', 'img_name'	
    ];

    public function statement()
	{
   		return $this->belongsTo('App\Statement');
	}
}
