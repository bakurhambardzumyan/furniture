<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\StatementSendMail;
use App\Mail\ContactUs;
use Illuminate\Support\Facades\Mail;

class MailSendController extends Controller
{
    public function sendMail(Request $request) 
    {
    	$data = $request->all();
    	
    	Mail::to('realtygood777@gmail.com')->send(new ContactUs($data));

		$success='ok';
    	return redirect()->back()->with('success',$success);
    }

    public function sendMailInStatement(Request $request) 
    {
    	$data = $request->all();
    	
    	Mail::to('realtygood777@gmail.com')->send(new StatementSendMail($data));

		$success='ok';
    	return redirect()->back()->with('success',$success);
    }
}
