<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Statement;
use Illuminate\Support\Facades\Input;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function agency()
    {
        $statements_top = Statement::where('top_news', 1)->take(6)->get();
        
        $statements_last = Statement::latest('id')->take(9)->get();
        
        $lang = app()->getLocale();

        return view('agency', ['statements_top' => $statements_top, 'statements_last' => $statements_last, 'lang' => $lang]);
    }

    public function furniture()
    {
        return view('furniture');
    }

    public function contact()
    {
        return view('contact');
    } 


    public function about()
    {
        return view('about');
    }


    public function statements(Request $request)
    {
        $type = $request->type;
        $lang = app()->getLocale();
        $part = $request->part;
        if ($type) {
            $statements = Statement::where('statement_type', $type)->paginate(2);
            if($part) {
                $statements = Statement::where('statement_type', $type)->where('statement_part', $part)
                            ->paginate(2);
            }
        } elseif ($part) {
            $statements = Statement::where('statement_part', $part)
                            ->paginate(2);
        } else {
            $statements = Statement::paginate(2);
        }
        return view('statements', ['statements' => $statements->appends(Input::except('page')), 'lang' => $lang, 'type' => $type, 'part' => $part]);
    }


    public function showStatement($lang, $id)
    {
        $statement = Statement::find($id);

        return view('show-statement', ['statement' => $statement, 'lang' => $lang]);
    }
}
