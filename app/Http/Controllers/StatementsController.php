<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Statement;
use App\Image;
use File;

class StatementsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statements = Statement::paginate(3);

        return view('statements.index', ['statements' => $statements, 'paginate' => true]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('statements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $statement = Statement::create($request->all());   
        $statement_id = $statement->id;
        if ($request->hasFile('images')) {
            $images = $request->file('images');
            $path = public_path().'/images/statements/' . $statement_id;
            if (!File::exists($path)) {
                File::makeDirectory($path, $mode = 0777, true, true);
            } 
            foreach ($images as $key => $image) {
                $name = time().'.'.$key.'.'.$image->getClientOriginalExtension();
                $image->move($path, $name);
                $image_create = 
                [
                    'statement_id' => $statement_id,
                    'img_name' => $name
                ];
                Image::create($image_create);
            }   
        }

        return redirect('admin/statement');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $statement = Statement::find($id);

        return view('statements.edit', ['statement' => $statement]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $statement = Statement::find($id);

        if ($statement) {
            $statement->update($request->all());
            if ($request->hasFile('images')) {
                $images = $request->file('images');
                $path = public_path().'/images/statements/' . $id;
                if (!File::exists($path)) {
                    File::makeDirectory($path, $mode = 0777, true, true);
                } 
                foreach ($images as $key => $image) {
                    $name = time().'.'.$key.'.'.$image->getClientOriginalExtension();
                    $image->move($path, $name);
                    $image_create = 
                    [
                        'statement_id' => $id,
                        'img_name' => $name
                    ];
                    Image::create($image_create);
                }   
            }
        }

        return redirect('/admin/statement');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $statement = Statement::find($id);
        if($statement) {
            $path = public_path().'/images/statements/' . $statement->id;
            if(\File::exists($path)) {
                \File::deleteDirectory($path);
            }

            $statement->images()->delete();
            $statement->delete();

            return 1;
        }
    }

    public function search(Request $request) 
    {
        $id = $request->id;
        $statements = Statement::where('id', $id)->get();
        $paginate = false;
        if(count($statements) == 0) {
            return redirect('/admin/statement');
        }


        return view('statements.index', ['statements' => $statements, 'paginate' => $paginate]);
    }
}
