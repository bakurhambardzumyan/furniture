<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Statement;
use App\Image;
use File;

class ImageController extends Controller
{
    public function deleteImg(Request $request)
    {
    	$imgId = $request->img_id;
    	$statementId = $request->statement_id;

    	$statement = Statement::find($statementId);
    	$img = Image::find($imgId);

    	if ($statement) {
    		$path = public_path().'/images/statements/' . $statementId.'/'.$img->img_name;
            if (File::exists($path)) {
            	File::delete($path);
            }
            $img->delete();

            return 1;
    	}
    }
}
