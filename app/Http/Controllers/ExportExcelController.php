<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\UsersExport;
use App\Statement;

class ExportExcelController extends Controller
{
	public function searchStatement()
	{
		return view('admin.statement-export');		
	}

    public function downloadExcel(Request $request)
    {
    	$statement_type = $request->statement_type;
    	$statement_part = $request->statement_part;
    	$region = $request->region;
    	$min_price = $request->min_price;
    	$max_price = $request->max_price;
    	$min_area = $request->min_area;
    	$max_area = $request->max_area;
    	$currency = $request->currency;
    	$min_floor = $request->min_floor;
    	$max_floor = $request->max_floor;

    	$statements = Statement::where(function($q) use ($statement_type, $statement_part, $region, $min_price, $max_price, $min_area, $max_area , $currency, $min_floor, $max_floor)  {

    		if($statement_type) {
    			$q->where('statement_type', $statement_type);
    		}

    		if($statement_part) {
    			$q->where('statement_part', $statement_part);
    		}

    		if($region) {
    			$q->where('region', $region);
    		}

    		if($region) {
    			$q->where('region', $region);
    		}

    		if($currency) {
    			$q->where('currency', $currency);
    		}

    		if($min_price) {
    			$q->where('price', '>=', $min_price);
    		}

    		if($max_price) {
    			$q->where('price', '<', $max_price);
    		}

    		if($min_area) {
    			$q->where('area', '>=', $min_area);
    		}

    		if($max_area) {
    			$q->where('area', '<', $max_area);
    		}

    		if($min_floor) {
    			$q->where('floor', '>=', $min_floor);
    		}

    		if($max_floor) {
    			$q->where('floor', '<', $max_floor);
    		}
   		})->select('id', 'statement_type', 'statement_part', 'region', 'price', 'currency', 'rooms', 'area', 'floor')->get();


		return Excel::download(new UsersExport($statements), 'statements.xlsx');
    }
}
