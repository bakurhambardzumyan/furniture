<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
	Route::get('/', 'AdminController@index')->name('admin');
	Route::resource('statement', 'StatementsController');
	Route::post('/delete-img', 'ImageController@deleteImg')->name('delete-img');
	Route::post('/search-statement-index', 'StatementsController@search')->name('search-statement-index');
	Route::get('/search-statement', 'ExportExcelController@searchStatement')->name('search-statement');
});

Route::redirect('/', '/en/agency');


Route::group(['prefix' => '{language}'], function () {
	Route::get('/', function () {
	    return view('agency');
	});

	// Auth::routes();

	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/furniture', 'HomeController@furniture')->name('furniture');
	Route::get('/agency', 'HomeController@agency')->name('agency');
	Route::get('/contact_us', 'HomeController@contact')->name('contact');
	Route::get('/about_us', 'HomeController@about')->name('about');
	Route::get('/statements', 'HomeController@statements')->name('statements');
	Route::get('/statements/{id}', 'HomeController@showStatement')->where('id', '[0-9]+')->name('statement-show');
});


Route::post('/send-mail', 'MailSendController@sendMail')->name('send-mail');
Route::post('/statement-send-mail', 'MailSendController@sendMailInStatement')->name('statement-send-mail');
// Route::post('/download', 'ExportExcelController@downloadExcel')->name('download-excel');
