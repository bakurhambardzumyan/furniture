@extends('layouts.app')


@section('content')
<div class="imageboxcontact"></div>
<div class="contact">
<h2 class="pt-5">@lang('messages.menu.contact_us')</h2>
</div>
<div class="container homepage">
  <a href="#" class="homeiconcontact"><i class="fas fa-home"></i>Home page</a>
</div>


<div class="form container mt-2">
<form method="post" action="{{ route('send-mail')}}">
  @csrf
  
  <div class="Touch mt-5">
  	<h3 class="get">@lang('messages.contact_us.get_in_touch')</h3>
  </div>


  <div class="md-form">
    <i class="fas fa-user prefix"></i>
    <input type="text" id="inputIconEx2" class="form-control" name="name" required>
    <label for="inputIconEx2">@lang('messages.contact_us.name')</label>
  </div>

  <div class="md-form">
    <i class="fas fa-at prefix"></i>
    <input type="email" id="inputValidationEx" class="form-control validate" name="email" required>
    <label for="inputValidationEx" data-error="wrong" data-success="right">       
      @lang('messages.contact_us.email')
    </label>
  </div> 

  <div class="md-form">
    <i class="fas fa-phone prefix"></i>
    <input type="tel" id="inputMDEx" class="form-control" name="phone" required>
    <label for="inputMDEx">@lang('messages.contact_us.phone')</label>
  </div>


  <div>
    <div class="md-form mb-0">
      <i class="fas fa-envelope prefix"></i>
      <textarea id="form-contact-message" class="form-control md-textarea" rows="3" name="text" required></textarea>
      <label for="form-contact-message">@lang('messages.contact_us.message')</label>
      <button class="send-mail sendmailcontactbtn" type="submit">
        <i class="far fa-paper-plane"></i>
      </button>
      </a>
    </div>
  </div>
</form>
<div class="follow">
	<h3 class="us mt-5">@lang('messages.contact_us.follow_us')</h3>

    <i class="icons fab fa-facebook-f"></i>
    <i class="icons fab fa-twitter"></i>
    <i class="icons fab fa-telegram-plane"></i>
    <i class="icons fab fa-vk"></i>

    <h3 class="support mt-5">@lang('messages.contact_us.our_data')</h3>
    	<h5 class="d-flex mt-3 ">Address : <p class="addres">Lavaca street,2900,USA</p></h5>
    	<h5 class="d-flex ">Email : <p class="addres">gevorgyan-henzel@mail.ru</p></h5>
    	<h5 class="d-flex ">Phone : <p class="addres">+374-93-61-33-63</p></h5>
    	<h5 class="d-flex ">Web : <p class="addres">www.Good-Realyty.com</p></h5>
</div>
</div>

<div class="sale mt-5">
	<h2>10% Off if you subscribe now</h2>
</div>
  @if(Session::get('success'))
    <input type="hidden" name="send-success" class="send-success" value="ok">
  @endif
  <div class="modal fade mail-success " id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
     <div class="modal-dialog modal-notify modal-success modal-top-right modal-side" role="document">
       <!--Content-->
       <div class="modal-content">
         <!--Header-->
         <div class="modal-header">
           <p class="heading lead">@lang('messages.contact_us.mail_sended')</p>

           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true" class="white-text">&times;</span>
           </button>
         </div>

         <!--Body-->
         <div class="modal-body">
           <div class="text-center">
             <i class="fas fa-check fa-4x mb-3 animated rotateIn"></i>
             <p>@lang('messages.contact_us.success_message_mail')</p>
           </div>
         </div>
     </div>
   </div>	
@endsection