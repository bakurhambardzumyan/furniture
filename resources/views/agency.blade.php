@extends('layouts.app')

@section('content')

	<!--Carousel Wrapper-->
<div class="carouselbox">

<div id="carousel-example-2" class="carousel slide carousel-fade" data-ride="carousel">
  <!--Indicators-->
  <ol class="carousel-indicators backgroundindicators">
    <li data-target="#carousel-example-2" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-2" data-slide-to="1"></li>
    <li data-target="#carousel-example-2" data-slide-to="2"></li>
  </ol>
  <!--/.Indicators-->
  <!--Slides-->
  <div class="carousel-inner" role="listbox">
    <div class="carousel-item active">
      <div class="view">
        <img class="d-block w-100" src="{{ asset('images/housecarouselbackgraound1.jpg') }}"
          alt="First slide">
        <div class="mask rgba-black-light"></div>
      </div>
      <div class="carousel-caption text-white">
        <h3 class="h3-responsive animated fadeInUp delay-2s slow">@lang('messages.agency.slide_text_1')
        </h3>
        <p class="animated fadeInDown delay-2s slow">@lang('messages.agency.slide_context_1')</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('images/housecarouselbackgraound2.jpg') }}"
          alt="Second slide">
        <div class="mask rgba-black-strong"></div>
      </div>
      <div class="carousel-caption text-white">
        <h3 class="h3-responsive animated fadeInUp delay-2s slow">
          @lang('messages.agency.slide_text_2')
        </h3>
        <p class="animated fadeInDown delay-2s slow">@lang('messages.agency.slide_context_2')</p>
      </div>
    </div>
    <div class="carousel-item">
      <!--Mask color-->
      <div class="view">
        <img class="d-block w-100" src="{{ asset('images/housecarouselbackgraound3.jpg') }}"
          alt="Third slide">
        <div class="mask rgba-black-slight"></div>
      </div>
      <div class="carousel-caption text-white">
        <h3 class="h3-responsive animated fadeInUp delay-2s slow">
          @lang('messages.agency.slide_text_3')
        </h3>
        <p class="animated fadeInDown delay-2s slow">@lang('messages.agency.slide_context_3')</p>
      </div>
    </div>
  </div>
  <!--/.Slides-->
  <!--Controls-->
  <a class="carousel-control-prev carouselcontroliconwidth" href="#carousel-example-2" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next carouselcontroliconwidth" href="#carousel-example-2" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
  <!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->
</div>
<div class="container">
	<!-- Top news -->
	<div class="topnews">
		<h2 class="mt-5 topnewsh2">@lang('messages.agency.top') <span class="h2span">@lang('messages.agency.news')</span></h2>
	</div>
</div>
<section class="details-card text-center sectagency">
    <div class="container">
        <div class="row">
          @if($statements_top)
            @foreach($statements_top as $key => $statement)
              <div class="col-md-4 {{ $key > 2 ? 'mt-5' : ''}}">
                  <div class="card-content">
                      <div class="card-img">
                          <img src="{{ '/images/statements/'.$statement->id .'/'. $statement->images[0]['img_name']}}" alt="" class="imgzoom">
                          <span><h4>@lang('messages.menu.'.$statement->statement_type)</h4></span>
                          <div class="d-flex justify-content-around align-items-center img-price"><h5>
                            {{ $statement->price }} {{ $statement->currency == 'usd' ? '$' : '֏' }}</h5><a href="#" data-toggle="tooltip" data-placement="top" title="Share" data-original-title="Share"><i class="fas fa-share-alt-square"></i></a></div>
                      </div>
                      <div class="card-desc">
                          <h3>{{ $statement[$lang.'_street'] }}</h3>
                          <p>{{ mb_substr($statement[$lang.'_text'], 0, 100) }}...</p>
                          <a href="statements/{{ $statement->id }}" class="btn-card">@lang('messages.agency.view_property')</a>   
                      </div>
                  </div>
              </div>
            @endforeach
          @endif
        </div>
    </div>
    <a href="statements">
      <button class="learntopnewsbutt mt-5">@lang('messages.agency.learn_more')</button>
    </a>
</section>

<!--Section: Content Services-->
<div class="z-depth-1 unique-color-dark serviceboxhome">
  <section class="text-center white-text serviceboxsection">

    <!-- Section heading -->
    <h2 class="font-weight-bold pt-3 pb-2 serviceshomeh2">@lang('messages.menu.services')</h2>
    <!-- Section description -->
    <p class="lead mx-auto mb-5">@lang('messages.agency.services_text')</p>

    <!-- Grid row -->
    <div class="row w-100">

      <!-- Grid column -->
      <div class="col-md-4 mb-4 serviceshome">
        <div class="houseiconservice">
          <a href="statements?type=&part=own_house">
            <img src="{{ asset('images/houseiconservice.png') }}" alt="houseiconservicegoodrealty" class="houseiconservicelogo">
          </a>
        </div>
        <h5 class="font-weight-bold my-4">@lang('messages.agency.home')</h5>

        <p class="mb-md-0 mb-5">
          @lang('messages.agency.home_text')
        </p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 mb-4 serviceshome">

        <div class="houseiconservice">
          <a href="statements?type=&part=apartment">
            <img src="{{ asset('images/hoteliconservice.png') }}" alt="hoteliconservicegoodrealty" class="houseiconservicelogo">
          </a>
        </div>        
        <h5 class="font-weight-bold my-4">@lang('messages.agency.apartment')</h5>
        <p class="mb-md-0 mb-5">
          @lang('messages.agency.apartment_text')
        </p>

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 mb-4 serviceshome">

        <div class="houseiconservice">
          <a href="statements?type=&part=land_area">
            <img src="{{ asset('images/landiconservice.png') }}" alt="landiconservicegoodrealty" class="houseiconservicelogo">
          </a>
        </div>
        <h5 class="font-weight-bold my-4">@lang('messages.agency.land')</h5>
        <p class="mb-0">
          @lang('messages.agency.apartment_text')
        </p>

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </section>
  <!--Section: Content-->


</div>
<div class="container mt-5">


  <!--Section: Content-->
  <section class="dark-grey-text text-center">
    
    <!-- Section heading -->
    <h3 class="mb-4 pb-2 lastnewsh3">@lang('messages.agency.last') <span class="h3span">@lang('messages.agency.news')</span></h3>
    <!-- Carousel Wrapper -->
    <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
      <!-- Controls -->
      <div class="controls-top mb-3">
        <a class="btn-floating waves-effect waves-light hoverable" href="#multi-item-example" data-slide="prev">
          <i class="fas fa-chevron-left"></i>
        </a>
        <a class="btn-floating waves-effect waves-light hoverable" href="#multi-item-example" data-slide="next">
          <i class="fas fa-chevron-right"></i>
        </a>
      </div>
      <!-- Controls -->
      <!-- Indicators -->
      <ol class="carousel-indicators mb-n5">
        <li class="success-color active" data-target="#multi-item-example" data-slide-to="0"></li>
        <li class="success-color" data-target="#multi-item-example" data-slide-to="1"></li>
        <li class="success-color" data-target="#multi-item-example" data-slide-to="2"></li>
      </ol>
      <!-- Indicators -->
      <!-- Slides -->
      <div class="carousel-inner" role="listbox">
        <!-- First slide -->
        <div class="carousel-item active">
          <div class="col-md-4 mb-2">
            <!-- Card -->
            <div class="card card-cascade narrower card-ecommerce">
              <!-- Card image -->
              <div class="view view-cascade overlay">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/img%20(39).jpg" class="card-img-top"
                  alt="sample photo">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <!-- Card image -->
              <!-- Card content -->
              <div class="card-body card-body-cascade text-center">
                <!-- Category & Title -->
                <a href="" class="text-muted">
                  <h5>Amiryan Str.</h5>
                </a>
                <h4 class="card-title my-4">
                  <strong>
                    <a href="" class="text-success">Center</a>
                  </strong>
                </h4>
                <!-- Description -->
                <p class="card-text">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                  consectetur, adipisci.
                </p>
                <!-- Card footer -->
                <div class="card-footer px-1">
                  <span class="float-left">45000$</span>
                  <span class="float-right">
                  <i class="fas fa-bed"></i>2Beds<i class="fas fa-layer-group"></i>120 sq.ft
                    <a class="material-tooltip-main" data-toggle="tooltip" data-placement="top" title="" data-original-title="Share">
                      <i class="fas fa-share-alt"></i>
                    </a>
                  </span>
                </div>
              </div>
              <!-- Card content -->
            </div>
            <!-- Card -->
          </div>
          <div class="col-md-4 mb-2 clearfix d-none d-md-block">
            <!-- Card -->
            <div class="card card-cascade narrower card-ecommerce">
              <!-- Card image -->
              <div class="view view-cascade overlay">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/img%20(22).jpg" class="card-img-top"
                  alt="sample photo">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <!-- Card image -->
              <!-- Card content -->
              <div class="card-body card-body-cascade text-center">
                <!-- Category & Title -->
                <a href="" class="text-muted">
                  <h5>Tigran-Petrosyant St.</h5>
                </a>
                <h4 class="card-title my-4">
                  <strong>
                    <a href="" class="text-success">Davitashen</a>
                  </strong>
                </h4>
                <!-- Description -->
                <p class="card-text">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                  consectetur, adipisci.
                </p>
                <!-- Card footer -->
                <div class="card-footer px-1">
                  <span class="float-left">45000$</span>
                  <span class="float-right">
                  <i class="fas fa-bed"></i> 2Beds <i class="fas fa-layer-group"></i> 120 sq.ft
                   <a class="material-tooltip-main" data-toggle="tooltip" data-placement="top" title="" data-original-title="Share">
                      <i class="fas fa-share-alt"></i>
                    </a>
                  </span>
                </div>
              </div>
              <!-- Card content -->
            </div>
            <!-- Card -->
          </div>
          <div class="col-md-4 mb-2 clearfix d-none d-md-block">
            <!-- Card -->
            <div class="card card-cascade narrower card-ecommerce">
              <!-- Card image -->
              <div class="view view-cascade overlay">
                <img src="https://mdbootstrap.com/img/Photos/Others/img%20(31).jpg" class="card-img-top" alt="sample photo">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <!-- Card image -->
              <!-- Card content -->
              <div class="card-body card-body-cascade text-center">
                <!-- Category & Title -->
                <a href="" class="text-muted">
                  <h5>Halabyan St.</h5>
                </a>
                <h4 class="card-title my-4">
                  <strong>
                    <a href="" class="text-success">Ajapnyak</a>
                  </strong>
                </h4>
                <!-- Description -->
                <p class="card-text">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                  consectetur, adipisci.
                </p>
                <!-- Card footer -->
                <div class="card-footer px-1">
                  <span class="float-left">45000$</span>
                  <span class="float-right">
                  <i class="fas fa-bed"></i>2Beds<i class="fas fa-layer-group"></i>120 sq.ft
                   <a class="material-tooltip-main" data-toggle="tooltip" data-placement="top" title="" data-original-title="Share">
                      <i class="fas fa-share-alt"></i>
                    </a>
                  </span>
                </div>
              </div>
              <!-- Card content -->
            </div>
            <!-- Card -->
          </div>
        </div>
        <!-- First slide -->
        <!-- Second slide -->
        <div class="carousel-item">
          <div class="col-md-4 mb-2">
            <!-- Card -->
            <div class="card card-cascade narrower card-ecommerce">
              <!-- Card image -->
              <div class="view view-cascade overlay">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/img%20(30).jpg" class="card-img-top"
                  alt="sample photo">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <!-- Card image -->
              <!-- Card content -->
              <div class="card-body card-body-cascade text-center">
                <!-- Category & Title -->
                <a href="" class="text-muted">
                  <h5>Lvovyan St.</h5>
                </a>
                <h4 class="card-title my-4">
                  <strong>
                    <a href="" class="text-success">Nor-Norq</a>
                  </strong>
                </h4>
                <!-- Description -->
                <p class="card-text">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                  consectetur, adipisci.
                </p>
                <!-- Card footer -->
                <div class="card-footer px-1">
                  <span class="float-left">45000$</span>
                  <span class="float-right">
                  <i class="fas fa-bed"></i>2Beds<i class="fas fa-layer-group"></i>120 sq.ft
                    <a class="material-tooltip-main" data-toggle="tooltip" data-placement="top" title="" data-original-title="Share">
                      <i class="fas fa-share-alt"></i>
                    </a>
                  </span>
                </div>
              </div>
              <!-- Card content -->
            </div>
            <!-- Card -->
          </div>
          <div class="col-md-4 mb-2 clearfix d-none d-md-block">
            <!-- Card -->
            <div class="card card-cascade narrower card-ecommerce">
              <!-- Card image -->
              <div class="view view-cascade overlay">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/img%20(37).jpg" class="card-img-top"
                  alt="sample photo">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <!-- Card image -->
              <!-- Card content -->
              <div class="card-body card-body-cascade text-center">
                <!-- Category & Title -->
                <a href="" class="text-muted">
                  <h5>Acharyan St.</h5>
                </a>
                <h4 class="card-title my-4">
                  <strong>
                    <a href="" class="text-success">Avan</a>
                  </strong>
                </h4>
                <!-- Description -->
                <p class="card-text">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                  consectetur, adipisci.
                </p>
                <!-- Card footer -->
                <div class="card-footer px-1">
                  <span class="float-left">45000$</span>
                  <span class="float-right">
                  <i class="fas fa-bed"></i>2Beds<i class="fas fa-layer-group"></i>120 sq.ft
                    <a class="material-tooltip-main" data-toggle="tooltip" data-placement="top" title="" data-original-title="Share">
                      <i class="fas fa-share-alt"></i>
                    </a>
                  </span>
                </div>
              </div>
              <!-- Card content -->
            </div>
            <!-- Card -->
          </div>
          <div class="col-md-4 mb-2 clearfix d-none d-md-block">
            <!-- Card -->
            <div class="card card-cascade narrower card-ecommerce">
              <!-- Card image -->
              <div class="view view-cascade overlay">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/img%20(31).jpg" class="card-img-top"
                  alt="sample photo">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <!-- Card image -->
              <!-- Card content -->
              <div class="card-body card-body-cascade text-center">
                <!-- Category & Title -->
                <a href="" class="text-muted">
                  <h5>Ayvazovski St.</h5>
                </a>
                <h4 class="card-title my-4">
                  <strong>
                    <a href="" class="text-success">Erebuni</a>
                  </strong>
                </h4>
                <!-- Description -->
                <p class="card-text">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                  consectetur, adipisci.
                </p>
                <!-- Card footer -->
                <div class="card-footer px-1">
                  <span class="float-left">45000$</span>
                  <span class="float-right">
                  <i class="fas fa-bed"></i>2Beds<i class="fas fa-layer-group"></i>120 sq.ft
                    <a class="material-tooltip-main" data-toggle="tooltip" data-placement="top" title="" data-original-title="Share">
                      <i class="fas fa-share-alt"></i>
                    </a>
                  </span>
                </div>
              </div>
              <!-- Card content -->
            </div>
            <!-- Card -->
          </div>
        </div>
        <!-- Second slide -->
        <!--Third slide -->
        <div class="carousel-item">
          <div class="col-md-4 mb-2 cardresponse">
            <!-- Card -->
            <div class="card card-cascade narrower card-ecommerce">
              <!-- Card image -->
              <div class="view view-cascade overlay">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/img%20(38).jpg" class="card-img-top"
                  alt="sample photo">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <!-- Card image -->
              <!-- Card content -->
              <div class="card-body card-body-cascade text-center">
                <!-- Category & Title -->
                <a href="" class="text-muted">
                  <h5>Shirak St.</h5>
                </a>
                <h4 class="card-title my-4">
                  <strong>
                    <a href="" class="text-success">Shengavit</a>
                  </strong>
                </h4>
                <!-- Description -->
                <p class="card-text">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                  consectetur, adipisci.
                </p>
                <!-- Card footer -->
                <div class="card-footer px-1">
                  <span class="float-left">45000</span>
                  <span class="float-right">
                  <i class="fas fa-bed"></i> 2Beds <i class="fas fa-layer-group"></i> 120 sq.ft
                    <a class="material-tooltip-main" data-toggle="tooltip" data-placement="top" title="" data-original-title="Share">
                      <i class="fas fa-share-alt"></i>
                    </a>
                  </span>
                </div>
              </div>
              <!-- Card content -->
            </div>
            <!-- Card -->
          </div>
          <div class="col-md-4 mb-2 clearfix d-none d-md-block">
            <!-- Card -->
            <div class="card card-cascade narrower card-ecommerce">
              <!-- Card image -->
              <div class="view view-cascade overlay">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/belt.jpg" class="card-img-top"
                  alt="sample photo">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <!-- Card image -->
              <!-- Card content -->
              <div class="card-body card-body-cascade text-center">
                <!-- Category & Title -->
                <a href="" class="text-muted">
                  <h5>Fanarjyan St.</h5>
                </a>
                <h4 class="card-title my-4">
                  <strong>
                    <a href="" class="text-success">Qanaqer-Zeytun</a>
                  </strong>
                </h4>
                <!-- Description -->
                <p class="card-text">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                  consectetur, adipisci.
                </p>
                <!-- Card footer -->
                <div class="card-footer px-1">
                  <span class="float-left">45000$</span>
                  <span class="float-right">
                  <i class="fas fa-bed"></i>2Beds<i class="fas fa-layer-group"></i>120 sq.ft
                    <a class="material-tooltip-main" data-toggle="tooltip" data-placement="top" title="" data-original-title="Share">
                      <i class="fas fa-share-alt"></i>
                    </a>
                  </span>
                </div>
              </div>
              <!-- Card content -->
            </div>
            <!-- Card -->
          </div>
          <div class="col-md-4 mb-2 clearfix d-none d-md-block">
            <!-- Card -->
            <div class="card card-cascade narrower card-ecommerce">
              <!-- Card image -->
              <div class="view view-cascade overlay">
                <img src="https://mdbootstrap.com/img/Photos/Horizontal/E-commerce/Products/img%20(57).jpg" class="card-img-top"
                  alt="sample photo">
                <a>
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
              <!-- Card image -->
              <!-- Card content -->
              <div class="card-body card-body-cascade text-center">
                <!-- Category & Title -->
                <a href="" class="text-muted">
                  <h5>Zoravar-Andranik St.</h5>
                </a>
                <h4 class="card-title my-4">
                  <strong>
                    <a href="" class="text-success">Malatia-Sebastia</a>
                  </strong>
                </h4>
                <!-- Description -->
                <p class="card-text">Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet,
                  consectetur, adipisci.
                </p>
                <!-- Card footer -->
                <div class="card-footer px-1">
                  <span class="float-left">45000$</span>
                  <span class="float-right">
                  <i class="fas fa-bed"></i>2Beds<i class="fas fa-layer-group"></i>120 sq.ft
                    <a class="material-tooltip-main" data-toggle="tooltip" data-placement="top" title="" data-original-title="Share">
                      <i class="fas fa-share-alt"></i>
                    </a>
                  </span>
                </div>
              </div>
              <!-- Card content -->
            </div>
            <!-- Card -->
          </div>
        </div>
        <!--Third slide -->
      </div>
      <!-- Slides -->
    </div>
    <!-- Carousel Wrapper -->
  </section>
  <!--Section: Content-->
</div>


<!-- Why choose us  -->

<div class="container my-5 p-5">


  <!--Section: Content-->
  <section class="dar-kgrey-text">

    <!-- Section heading -->
    <h2 class="text-center font-weight-bold mb-4 pb-2 whychooseush2">@lang('messages.agency.why_choose_use')</h2>
    <!-- Section description -->
    <p class="text-center lead grey-text mx-auto mb-5 whychooseusp">
      @lang('messages.agency.why_choose_use_text')
    </p>

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-4">

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-2">
            <i class="fas fa-2x fa-flag-checkered deep-purple-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3 whychoosehover1h5">
              @lang('messages.agency.mortgage_loan')
            </h5>
            <p class="grey-text whychoosehover1p">
              @lang('messages.agency.mortgage_loan_text')
            </p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-2">
            <i class="fas fa-2x fa-flask deep-purple-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3 whychoosehover2h5">@lang('messages.agency.experienced_professionals')
            </h5>
            <p class="grey-text whychoosehover2p">
              @lang('messages.agency.experienced_professionals_text')
            </p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row mb-md-0 mb-3">

          <!-- Grid column -->
          <div class="col-2">
            <i class="fas fa-2x fa-glass-martini deep-purple-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3 whychoosehover3h5">@lang('messages.agency.diversity')</h5>
            <p class="grey-text mb-md-0 whychoosehover3p">
              @lang('messages.agency.diversity_text')
            </p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4 text-center chooseushover">
        <img class="img-fluid" src="{{ asset('images/houseinhandhome.png') }}"
          alt="Sample image">
      </div>
      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-4">

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-2">
            <i class="far fa-2x fa-heart deep-purple-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3 whychoosehover4h5">
              @lang('messages.agency.work_experience')
            </h5>
            <p class="grey-text whychoosehover4p">@lang('messages.agency.work_experience_text')</p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row mb-3">

          <!-- Grid column -->
          <div class="col-2">
            <i class="fas fa-2x fa-bolt deep-purple-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3 whychoosehover5h5">
              @lang('messages.agency.individual_approach')
            </h5>
            <p class="grey-text whychoosehover5p">
              @lang('messages.agency.individual_approach_text')
            </p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

        <!-- Grid row -->
        <div class="row">

          <!-- Grid column -->
          <div class="col-2">
            <i class="fas fa-2x fa-magic deep-purple-text"></i>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-10">
            <h5 class="font-weight-bold mb-3 whychoosehover6h5">  
              @lang('messages.agency.design')
            </h5>
            <p class="grey-text mb-0 whychoosehover6p">
              @lang('messages.agency.design_text')
            </p>
          </div>
          <!-- Grid column -->

        </div>
        <!-- Grid row -->

      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </section>
  <!--Section: Content-->
</div>
<!-- Why choouse Us end -->
<!-- Client say box -->
<div class="container mt-5">
  <div class="clientsaybox">
  <div class="row">
    <div class="col-sm-12">     
      <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <h2 class="clientsay">What Clients <b>Say</b></h2>
        <!-- Carousel indicators -->
        <ol class="carousel-indicators clientsaysindicator">
          <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
          <li data-target="#myCarousel" data-slide-to="1"></li>
          <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>   
        <!-- Wrapper for carousel items -->
        <div class="carousel-inner">
          <div class="item carousel-item active animated fadeInUp delay-1s">
            <div class="row">
              <div class="col-sm-6">
                <div class="testimonial ">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.</p>
                </div>
                <div class="media">
                  <div class="media-left d-flex mr-3">
                    <img src="{{ asset('images/clientsayimg.jpg') }}" alt="">                   
                  </div>
                  <div class="media-body">
                    <div class="overview">
                      <div class="name"><b>Paula Wilson</b></div>                  <div class="star-rating">
                        <ul class="list-inline mt-2">
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                        </ul>
                      </div>
                    </div>                    
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="testimonial">
                  <p>Vestibulum quis quam ut magna consequat faucibu. Eget mi suscipit tincidunt. Utmtc tempus dictum. Pellentesque virra. Quis quam ut magna consequat faucibus quam.</p>
                </div>
                <div class="media">
                  <div class="media-left d-flex mr-3">
                    <img src="{{ asset('images/clientsayimg.jpg') }}" alt="">
                  </div>
                  <div class="media-body">
                    <div class="overview">
                      <div class="name"><b>Antonio Moreno</b></div>                   <div class="star-rating">
                        <ul class="list-inline mt-2">
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                        </ul>
                      </div>
                    </div>                    
                  </div>
                </div>
              </div>
            </div>      
          </div>
          <div class="item carousel-item animated fadeInDown delay-1s">
            <div class="row">
              <div class="col-sm-6">
                <div class="testimonial ">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.</p>
                </div>
                <div class="media">
                  <div class="media-left d-flex mr-3">
                    <img src="{{ asset('images/clientsayimg.jpg') }}" alt="">                   
                  </div>
                  <div class="media-body">
                    <div class="overview">
                      <div class="name"><b>Michael Holz</b></div>                  
                      <div class="star-rating">
                        <ul class="list-inline mt-2">
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                        </ul>
                      </div>
                    </div>                    
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="testimonial">
                  <p>Vestibulum quis quam ut magna consequat faucibu. Eget mi suscipit tincidunt. Utmtc tempus dictum. Pellentesque virra. Quis quam ut magna consequat faucibus quam.</p>
                </div>
                <div class="media">
                  <div class="media-left d-flex mr-3">
                    <img src="{{ asset('images/clientsayimg.jpg') }}" alt="">                   
                  </div>
                  <div class="media-body">
                    <div class="overview">
                      <div class="name"><b>Mary Saveley</b></div>
                      <div class="star-rating mt-2">
                        <ul class="list-inline">
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                        </ul>
                      </div>
                    </div>                    
                  </div>
                </div>
              </div>
            </div>      
          </div>
          <div class="item carousel-item animated fadeInUp delay-1s">
            <div class="row">
              <div class="col-sm-6">
                <div class="testimonial ">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam eu sem tempor, varius quam at, luctus dui. Mauris magna metus, dapibus nec turpis vel, semper malesuada ante.</p>
                </div>
                <div class="media">
                  <div class="media-left d-flex mr-3">
                    <img src="{{ asset('images/clientsayimg.jpg') }}" alt="">                   
                  </div>
                  <div class="media-body">
                    <div class="overview">
                      <div class="name"><b>Martin Sommer</b></div>                    
                      <div class="star-rating">
                        <ul class="list-inline mt-2">
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                        </ul>
                      </div>
                    </div>                    
                  </div>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="testimonial">
                  <p>Vestibulum quis quam ut magna consequat faucibu. Eget mi suscipit tincidunt. Utmtc tempus dictum. Pellentesque virra. Quis quam ut magna consequat faucibus quam.</p>
                </div>
                <div class="media">
                  <div class="media-left d-flex mr-3">
                    <img src="{{ asset('images/clientsayimg.jpg') }}" alt="">                   
                  </div>
                  <div class="media-body">
                    <div class="overview">
                      <div class="name"><b>John Williams</b></div>
                      <div class="star-rating">
                        <ul class="list-inline mt-2">
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star"></i></li>
                          <li class="list-inline-item"><i class="fa fa-star-o"></i></li>
                        </ul>
                      </div>
                    </div>                    
                  </div>
                </div>
              </div>
            </div>      
          </div>
        </div>
        <!-- Carousel controls -->
        <a class="carousel-control left carousel-control-prev carouselcontrolclientsayleft carouselcontrolclient" href="#myCarousel" data-slide="prev">
          <i class="fa fa-chevron-left"></i>
        </a>
        <a class="carousel-control right carousel-control-next carouselcontrolclientsaynext carouselcontrolclient" href="#myCarousel" data-slide="next">
          <i class="fa fa-chevron-right"></i>
        </a>
      </div>
    </div>
  </div>
</div>
</div>
<!-- Client say box end -->
@endsection