@extends('layouts.app')

@section('content')

<div class="fullimages"></div>
<div class="container mt-5">
 <h2>{{ $statement[$lang.'_title'] }}</h2>
 <h3>{{ $statement->price }} {{ $statement->currency == 'usd' ? '$' : '֏' }}</h3>
</div>
<div class="container fullimage ">
<div class="carousel-container position-relative row ">
  
<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    @foreach($statement->images as $key => $image)
    <div class="carousel-item {{$key == 0 ? 'active' : '' }}" data-slide-number="{{ $key }}">
      <img src="{{ '/images/statements/'.$statement->id.'/'.$image['img_name']}}" class="d-block w-100" alt="good realty img" data-remote="https://source.unsplash.com/Pn6iimgM-wo/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
    </div>
    @endforeach
    <!-- <div class="carousel-item active" data-slide-number="0">
      <img src="https://source.unsplash.com/tXqVe7oO-go/1600x900/" class="d-block w-100" alt="good realty img" data-remote="https://source.unsplash.com/tXqVe7oO-go/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
    </div>
    <div class="carousel-item" data-slide-number="1">
      <img src="https://source.unsplash.com/qlYQb7B9vog/1600x900/" class="d-block w-100" alt="good realty img" data-remote="https://source.unsplash.com/qlYQb7B9vog/" data-type="image" data-toggle="lightbox" data-gallery="example-gallery">
    </div> -->
  </div>
</div>
<!-- Carousel Navigation -->
<div id="carousel-thumbs" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <div class="row mx-0">
        @foreach($statement->images as $key => $image)
          <div id="carousel-selector-{{ $key }}" class="thumb col-4 col-sm-2 px-1 py-2 selected" data-target="#myCarousel" data-slide-to="{{ $key }}">
            <img src="{{ '/images/statements/'.$statement->id .'/'. $image['img_name']}}" class="img-fluid" alt="good realty img">
          </div>
        @endforeach
        <!-- <div id="carousel-selector-0" class="thumb col-4 col-sm-2 px-1 py-2" data-target="#myCarousel" data-slide-to="1">
          <img src="https://source.unsplash.com/tXqVe7oO-go/600x400/" class="img-fluid" alt="good realty img">
        </div>
        <div id="carousel-selector-1" class="thumb col-4 col-sm-2 px-1 py-2" data-target="#myCarousel" data-slide-to="2">
          <img src="https://source.unsplash.com/qlYQb7B9vog/600x400/" class="img-fluid" alt="good realty img">
        </div> -->
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carousel-thumbs" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel-thumbs" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>

</div> <!-- /row -->
</div> <!-- /container -->
<div class="container inpormation pt-5 my-5 z-depth-1">
  <h4 class="text-dark">Center,Amiryan St.</h4>
<div class="text-start">
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modalRegular"> Location</button>
</div>

<!--Modal: Name-->
<div class="modal fade" id="modalRegular" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">

    <!--Content-->
    <div class="modal-content">

      <!--Body-->
      <div class="modal-body mb-0 p-0">

        <!--Google map-->
        <div id="map-container-google-16" class="z-depth-1-half map-container-9" style="height: 400px">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3048.2779906228193!2d44.50598941488674!3d40.18062647788977!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x406abcfc30449aa1%3A0x28ecbb9100ad3e80!2z1LHVtNWr1oDVtdWh1bYg1oPVuNWy1bjWgSwg1LXWgNaH1aHVtiwg1YDVodW11aHVvdW_1aHVtg!5e0!3m2!1shy!2s!4v1585445053444!5m2!1shy!2s" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
            frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

      </div>  

      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-outline-success btn-md" data-dismiss="modal">Close <i class="fas fa-times ml-1"></i></button>

      </div>

    </div>
    <!--/.Content-->

  </div>
</div>
<!--Modal: Name-->

<!--Modal: Name-->
<div class="modal fade" id="modalSatellite" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">

    <!--Content-->
    <div class="modal-content">

      <!--Body-->
      <div class="modal-body mb-0 p-0">

        <!--Google map-->
        <div id="map-container-google-17" class="z-depth-1-half map-container-10" style="height: 400px">
          <iframe src="https://maps.google.com/maps?q=new%20york&t=k&z=13&ie=UTF8&iwloc=&output=embed"
            frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">

        <button type="button" class="btn btn-outline-default btn-md" data-dismiss="modal">Close <i class="fas fa-times ml-1"></i></button>

      </div>

    </div>
    <!--/.Content-->

  </div>
</div>
  <section class="p-md-3 mx-md-5">
    <div class="row pt-3">
      <div class="col-lg-3 col-md-6 mb-5">
        <h4 class="font-weight-bold mb-3">
         <i class="fas fa-igloo"></i> @lang('messages.show_statement.building_type')
        </h4>
        <p class="text-muted mb-lg-0">
         <strong> @lang('messages.building_types.'.$statement->building_type)</strong>
        </p>
      </div>
      <div class="col-lg-3 col-md-6 mb-5">
        <h4 class="font-weight-bold mb-3">
          <i class="fas fa-bed roomsicon"></i> @lang('messages.show_statement.room')
        </h4>
        <p class="text-muted mb-lg-0">
          <strong>4</strong>
        </p>
      </div>
      <div class="col-lg-3 col-md-6 mb-5">
        <h4 class="font-weight-bold mb-3">
        <i class="fa fa-layer-group indigo-text"></i> @lang('messages.show_statement.space')
        </h4>
        <p class="text-muted mb-md-0">
         <strong>118 m2</strong>
        </p>
      </div>
      <div class="col-lg-3 col-md-6 mb-5">
        <h4 class="font-weight-bold mb-3">
        <i class="fas fa-building"></i> @lang('messages.show_statement.floor')
        </h4>
        <p class="text-muted mb-md-0">
         <strong> 5/2</strong>
        </p>
      </div>
    </div>
    <div class="row d-flex justify-content-between align-items-center">
      <div class="col-md-6 mb-4">
        <h2 class="font-weight-bold mb-3">
          <i class="fas fa-info-circle"></i>
          @lang('messages.show_statement.information')  
        </h2>
        <p class="text-muted pt-3">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
          enim ad minim veniam, quis nostrud exercitation ullamco laboris
          nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
          in reprehenderit in voluptate velit esse cillum dolore eu fugiat
          nulla pariatur. Excepteur sint occaecat cupidatat non proident,
          sunt in culpa qui officia deserunt mollit anim id est laborum.
        </p>
      </div>
      <div class="col-md-6 col-lg-4  justify-content-center mb-md-0 mb-5">
        
        <div class="modal fade" id="modalContactForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <form method="post" action="{{ route('statement-send-mail')}}">
  @csrf
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header text-center">
          <h4 class="modal-title w-100 font-weight-bold">@lang('messages.contact_us.get_in_touch')</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body mx-3">
          <div class="md-form mb-5">
            <i class="fas fa-phone prefix"></i>
            <input type="tel" maxlength="15" onkeydown="return phoneKey(event.key)" id="form34" class="form-control validate" name="phone" required>
            <label data-error="wrong" data-success="right" for="form15">@lang('messages.contact_us.phone')</label>
          </div>

          <div class="md-form mb-5">
            <i class="fas fa-at prefix"></i>
            <input type="email" maxlength="30" id="form29" class="form-control validate" name="email" required>
            <label data-error="wrong" data-success="right" for="form29">@lang('messages.contact_us.email')</label>
          </div>

         <div class="md-form form-sm">
            <i class="fa fa-tag prefix"></i>
            <input type="number" maxlength="5" id="materialFormSubjectModalEx1" class="form-control form-control-sm" name="reference" required value="{{ $statement->id }}">
            <label for="materialFormSubjectModalEx1">@lang('messages.contact_us.ad_reference')</label>
          </div>

          <div class="md-form">
            <i class="fas fa-envelope prefix"></i>
            <textarea type="text" id="form8" maxlength="500" class="md-textarea form-control" rows="4" name="text" required></textarea>
            <label data-error="wrong" data-success="right" for="form8">@lang('messages.contact_us.message')</label>
          </div>

        </div>
        <div class="modal-footer d-flex justify-content-center">
          <button class="sendbutton sendmailcontactbtn">@lang('messages.contact_us.send')</button>
        </div>
      </div>
    </div>
  </form>
</div>

<div class="text-center">
  <a href="" class="btn btn-success btn-rounded mb-4" data-toggle="modal" data-target="#modalContactForm">Contact</a>
</div>


  </div>
</div>

<div class="row">
  <div class="col text-right">
    <p class="float-right"> Renewed: <strong>March 28, 2020</strong></p>
    <p class="float-right mr-4"> Date: <strong>{{ date('d-m-Y', strtotime($statement->created_at)) }}</strong></p>
    <p class="float-right mr-4">Ad reference:  <strong>{{ $statement->id }}</strong></p>
  </div>
</div>
    
  </section>
</div>

@if(Session::get('success'))
    <input type="hidden" name="send-success" class="send-success" value="ok">
  @endif
  <div class="modal fade mail-success " id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
     <div class="modal-dialog modal-notify modal-success modal-top-right modal-side" role="document">
       <!--Content-->
       <div class="modal-content">
         <!--Header-->
         <div class="modal-header">
           <p class="heading lead">@lang('messages.contact_us.mail_sended')</p>

           <button type="button" class="close" data-dismiss="modal" aria-label="Close">
             <span aria-hidden="true" class="white-text">&times;</span>
           </button>
         </div>

         <!--Body-->
         <div class="modal-body">
           <div class="text-center">
             <i class="fas fa-check fa-4x mb-3 animated rotateIn"></i>
             <p>@lang('messages.contact_us.success_message_mail')</p>
           </div>
         </div>
     </div>
   </div> 
@endsection