@extends('layouts.app')


@section('content')



<!--Main layout-->
      <!--Section: Jumbotron-->
      <section class="card  wow fadeIn forsalebladeimg" id="intro">
        
        <div class="card-body text-white text-center py-5 px-5 my-5">

          <h1 class="mb-4">
            <strong>The Full Sale Ads</strong>
          </h1>
          <p>
            <strong>Best & free guide of responsive web design</strong>
          </p>
          <p class="mb-4">
            <strong>Good-Realty Real Estate Agency is always ready to help you to achieve the best results in buying, selling or renting real estate in Armenia. </strong>
          </p>
        </div>
        <!-- Content -->
        <!-- Content -->
      </section>
      <!--Section: Jumbotron-->
    <div class="container">
   <div class="container my-5 px-5 pt-5 pb-1 z-depth-1">


  <!--Section: Content-->
  <section class="text-center">
   
    <!-- Section heading -->
    <h2 class="font-weight-bold mb-5">Regular Ads</h2>
    <!-- Section description -->
    <p class="lead grey-text mx-auto mb-5">We offer the best requirements for our clients.</p>

    <!-- Grid row -->
    <div class="row mb-5">

      <!-- Grid column -->
      <div class="col-12">

        <ul class="nav md-pills justify-content-around pills-rounded pills-deep-purple forsalenavbar">
          <li class="nav-item">
            <a class="nav-link statement-part {{ $part == 'apartment' ? 'bg-success text-white' : '' }}" href="{{ url()->current() }}?type={{ $type }}&part=apartment">
              @lang('messages.statement_part.apartment')
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link statement-part {{ $part == 'own_house' ? 'bg-success text-white' : '' }}" href="{{ url()->current() }}?type={{ $type }}&part=own_house">
              @lang('messages.statement_part.own_house')
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link statement-part {{ $part == 'commercial_area' ? 'bg-success text-white' : '' }}" href="{{ url()->current() }}?type={{ $type }}&part=commercial_area">
              @lang('messages.statement_part.commercial_area')
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link statement-part {{ $part == 'land_area' ? 'bg-success text-white' : '' }}" href="{{ url()->current() }}?&type={{ $type }}&part=land_area">
              @lang('messages.statement_part.land_area')
            </a>
          </li>
        </ul>

        <!-- Tab panels -->
        <div class="tab-content">

          <!--Panel 1-->
          <div class="tab-pane fade in show active" id="panel100" role="tabpanel">

    <div class="view rounded ">
      <section class="pt-5">
        @if(count($statements) > 0)        
          @foreach($statements as $statement)        
            <div class="row mt-3 mb-2 border-bottom wow fadeIn">
              <div class="col-lg-5 col-xl-4 mb-4">
                <div class="view overlay rounded z-depth-1">
                  <img src="{{ '/images/statements/'.$statement->id .'/'. $statement->images[0]['img_name']}}" class="img-fluid statement-img" alt="">
                  <a href="statements/{{ $statement->id }}">
                    <div class="mask rgba-white-slight"></div>
                  </a>
                </div>  
              </div>
              <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4 cardtextforsale">
                <h2 class="mb-3 font-weight-bold dark-grey-text">
                  {{ $statement[$lang.'_title'] }}
                </h2>
                <h3 class="forsalestreet text-bold"><i class="fas fa-map-marker-alt"></i> {{ $statement[$lang.'_street'] }}</h3>
                <p class=""><i class="fas fa-coins"></i> @lang('messages.show_statement.price'): <strong>{{ $statement->price }} 
                  {{ $statement->currency == 'usd' ? '$' : '֏' }}</strong> </p>
                <div class="infocardforsale">
                  @if($statement->rooms)
                    <p class="floorscard"><i class="fas fa-bed"></i> @lang('messages.show_statement.room'): <strong>{{ $statement->rooms }}</strong></p>  
                  @endif
                  <p class="livingspacecard"><i class="fas fa-chart-area"></i> @lang('messages.show_statement.space'): <strong>{{ $statement->area }}m2</strong></p>
              	</div>
              </div>
            </div>
          @endforeach  
        @else
          <dir class="container text-center mt-3">
            <h4 class="text-success">Այս պահին Ձեր նշված բաժնում հայտարարություն չկա !</h4>
          </dir>
        @endif  
        <!--Pagination-->
        <nav class="d-flex justify-content-center wow fadeIn">
              {{ $statements->links() }}
        </nav>
        <!--Pagination-->

      </section>
      <!--Section: Cards-->
            </div>

          </div>
          <!--/.Panel 1-->
        </div>
      </div>
      <!-- Grid column -->
    </div>
    <!-- Grid row -->
  </section>
  <!--Section: Content-->

</div>
</div>
  <!--Main layout-->



@endsection