@extends('layouts.app')

@section('content')
	<div class="container mt-100">
		<form method="POST" action="/download">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="row mt-5">
				<div class="col-md-4 select-outline">
				    <select class="browser-default custom-select" name="statement_type">
				      <option class="text-dark" value="" disabled selected>Ընտրել հայտարարության տեսակը</option>
				      <option class="text-dark" value="for_rent">Վարձակալության</option>
				      <option class="text-dark" value="for_sale">Վաճառքի</option>
				    </select>
				</div>
				<div class="col-md-4 select-outline">
				    <select class="browser-default custom-select statement_part" name="statement_part">
				      <option class="text-dark" value="" disabled selected>Ընտրել հայտարարության բաժինը</option>
				      <option class="text-dark" value="own_house">Սեփական տուն</option>
				      <option class="text-dark" value="apartment">Բնակարան</option>
				      <option class="text-dark" value="commercial_area">Կոմերցիոն տարածք</option>
				      <option class="text-dark" value="land_area">Հողատարածք</option>
				    </select>
				</div>
				<div class="col-md-4 select-outline">
				    <select class="browser-default custom-select" name="region" required>
				      <option class="text-dark" value="" disabled selected>Տարածաշրջան</option>
				      <option class="text-dark" value="ajapnyak">Աջափնյակ</option>
				      <option class="text-dark" value="arabkir">Արաբկիր</option>
				      <option class="text-dark" value="avan">Ավան</option>
				      <option class="text-dark" value="davtashen">Դավիթաշեն</option>
				      <option class="text-dark" value="erebuni">Էրեբունի</option>
				      <option class="text-dark" value="qanaqer_zeytun">Քանաքեռ Զեյթուն</option>
				      <option class="text-dark" value="kentron">Կենտրոն</option>
				      <option class="text-dark" value="malatya_sebastia">Մալաթիա Սեբաստիա</option>
				      <option class="text-dark" value="nor_norq">Նոր Նորք</option>
				      <option class="text-dark" value="norq_marash">Նորք Մարաշ</option>
				      <option class="text-dark" value="nubarashen">Նուբարաշեն</option>
				      <option class="text-dark" value="shengavit">Շենգավիթ</option>
				    </select>  
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-4">
					<div class="md-form mb-0">
						<span class="mr-3">Գինը</span>
					   	<input name="min_price" type="number" min="0" placeholder="մին" class="price-inputs">
					   	<input name="max_price" type="number" min="0" placeholder="մաքս" class="price-inputs">
					</div>
				</div>
				<div class="col-4">
					<div class="md-form mb-0">
						<span class="mr-3">Մակերեսը</span>
					   	<input name="min_area" type="number" min="0" placeholder="մին" class="price-inputs">
					   	<input name="max_area" type="number" min="0" placeholder="մաքս" class="price-inputs">
					</div>
				</div>
				<div class="col-2 select-outline mt-auto">
				    <select class="browser-default custom-select" name="currency">
				      <option class="text-dark" value="" disabled selected>Տարադրամ</option>
				      <option class="text-dark" value="usd">USD</option>
				      <option class="text-dark" value="amd">AMD</option>
				    </select>
				</div>	
			</div>
			<div class="row mt-5">
				<div class="col-4">
					<div class="md-form mb-0">
						<span class="mr-3">Հարկ</span>
					   	<input name="min_floor" type="number" min="0" placeholder="մին" class="price-inputs">
					   	<input name="max_floor" type="number" min="0" placeholder="մաքս" class="price-inputs">
					</div>
				</div>
				<div class="col-8">
					<div class="text-right">
						<button type="submit" class="btn btn-success">Ներբեռնել Excel</button>
					</div>
				</div>
			</div>
		</form>
	</div>
@endsection