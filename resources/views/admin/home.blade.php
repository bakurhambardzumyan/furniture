@extends('layouts.app')

@section('content')
	<div class="mt-100 text-center">
		<h1 class="text-success">Admin Panel</h1>	
	</div>
	<div class="mt-3 text-center">
		<a href="admin/statement">
			<button type="button" class="btn btn-success">Հայտարարություններ</button>
		</a>
	</div>
@endsection