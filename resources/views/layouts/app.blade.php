<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Good Reality</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="{{ asset('js/script.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">


    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/aboutstyle.css') }}" rel="stylesheet">
    <link href="{{ asset('css/contactstyle.css') }}" rel="stylesheet">
    <link href="{{ asset('css/homestyle.css') }}" rel="stylesheet">
    <link href="{{ asset('css/flaticon.css') }}" rel="stylesheet">
    <link href="{{ asset('css/forsale.css') }}" rel="stylesheet">
    <link href="{{ asset('css/fullimgcards.css') }}" rel="stylesheet">
    <!-- Font Awesome -->

    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.14.1/css/mdb.min.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
</head>
<body>
    <div id="app">
            
<div class="container d-flex justify-content-end continfobox">
  <p class="numberheader"><img src="{{ asset('images/callicon.png') }}" alt="call"> + 37498 33 23 83</p>
  <p class="numberheader ml-3"><img src="{{ asset('images/emailicon.png') }}" alt="email"> misakyan.gevorg99@gmail.com</p>
</div>
<header class="container">
<!-- header start -->
<!-- nav start -->
<nav class="navbar fixed-top navbar-fixed-top navbar-expand-lg scrolling-navbar mt-4"><div class="navbar-brandbef"></div>
  <a class="navbar-brand position-relative" href="#"><img src="{{ asset('images/logo.png') }}" title="Good-Realty" class="good-realtylogo" alt="goodrealty logo"
    class=""></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="#"> @lang('messages.menu.home') <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="about_us">@lang('messages.menu.about_us')</a>
      </li>
      <li class="nav-item dropdown dropdownli">
        <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true"
          aria-expanded="false">@lang('messages.menu.services')
        </a>
        <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
          <a class="dropdown-item" href="statements?type=for_sale">@lang('messages.menu.for_sale')</a>
          <a class="dropdown-item" href="statements?type=for_rent">@lang('messages.menu.for_rent')</a>
        </div>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="contact_us">@lang('messages.menu.contact_us')</a>
      </li>
    </ul>
    <form class="form-inline">
      <div class="md-form my-0">
      </div>
    </form>
  </div>
</nav>
<div class="myBtncontrol">
<button onclick="topFunction()" id="myBtn"><img src="{{ asset('images/up-arrow.png') }}" alt="BtnUp" class="myBtnup">@lang('messages.footer.top')</button></div>


</header>
<!-- nav,header end-->




        </div>
<!--Main layout-->

            <main class="">
                @yield('content')
            </main>
    <footer>
<section class="nb-footer">
<div class="footbg">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-6 footer-singlelogo">
  <div class="footer-singlelogo">
   <img src="{{ asset('images/logo.png') }}" class="img-responsive" alt="Logo Good-Realty">
  <div class="dummy-logo">
    <h2>Good Reality</h2>
  </div>

    <p>@lang('messages.footer.text_good_reality')</p>
    <a href="about_us" class="btn btn-footer">@lang('messages.footer.read_more')</a>
  </div>
</div>

<div class="col-md-3 col-sm-6">
  <div class="footer-single useful-links">
   <div class="footer-title"><h2>Good Realty</h2></div>
   <ul class="list-unstyled listfootul">
            <li>
              <a href="">
                @lang('messages.menu.home') 
                <i class="fa fa-angle-right pull-right"></i>
              </a>
            </li>
            <li>
              <a href="about_us">
                @lang('messages.menu.about_us') 
                <i class="fa fa-angle-right pull-right"></i>
              </a>
            </li>
            <li>
              <a href="statements?type=for_sale">
                @lang('messages.menu.for_sale')
                 <i class="fa fa-angle-right pull-right"></i>
               </a>
             </li> 
            <li>
              <a href="statements?type=for_rent">
                @lang('messages.menu.for_rent')
                <i class="fa fa-angle-right pull-right"></i>
              </a>
            </li>
            <li>
              <a href="contact_us">
                @lang('messages.menu.contact_us') 
                <i class="fa fa-angle-right pull-right"></i>
              </a>
            </li>
          </ul>
        </div>
</div>
<div class="clearfix visible-sm"></div>

<div class="col-md-3 col-sm-6">
  
  <div class="left-clear right-clear footer-single footer-project">
    <div class="footer-title"><h2>@lang('messages.footer.opening_hours')</h2></div>
      <div class="ophours">
        <div class="text-start">
        <a href="" class="text-success" data-toggle="modal" data-target="#modalRegular"><i class="fas fa-map-marker-alt"></i> Center, Amiryan Street</a>
        </div>
<!--Modal: Name-->
        <p><i class="far fa-calendar-alt"></i> @lang('messages.footer.monday_sunday')</p>
        <p><i class="fas fa-clock"></i> 10:00-19:00</p>
        <p><i class="fas fa-headset"></i> @lang('messages.footer.support') - 24/7</p>
      </div>
  </div>
<div class="modal fade" id="modalRegular" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Body-->
      <div class="modal-body mb-0 p-0">
        <!--Google map-->
        <div id="map-container-google-16" class="z-depth-1-half map-container-9" style="height: 400px">
          <iframe src="https://maps.google.com/maps?q=new%20delphi&t=&z=13&ie=UTF8&iwloc=&output=embed"
            frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
      <!--Footer-->
      <div class="modal-footer justify-content-center">
        <button type="button" class="btn btn-outline-success btn-md" data-dismiss="modal">Close <i class="fas fa-times ml-1"></i></button>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>

</div>

<div class="col-md-3 col-sm-6 footer-conticons">
  <div class="footer-single">
    <div class="footer-title footer-titleh2block">
      <h2 class="continfoh2foot">
        @lang('messages.footer.contact_information')
      </h2>
    </div>
    <address>
      <a href="tel: +37498332383"><i class="fas fa-mobile-alt ml-1"></i> +374 98 33 23 83</a><br>
      <a href="tel: +37410621771"><i class="fa fa-fax"></i> +374 10 62 17 71</a><br>
      <a href="viber://091231231" class="viberfoot"><i class="fab fa-viber"></i> +374 91 23 12 31</a><br>
      <a href="whatsapp://+077331233" class="whatsappfoot"><i class="fab fa-whatsapp"></i> +374 77 33 12 33</a><br>

      <p><i class="fa fa-envelope"></i> realtygood777@gmail.com</p>
    </address>          
  </div>
</div>

</div>
</div>
</section>
<section class="nb-copyright">
<div class="container">
<div class="row">
<div class="col-sm-6 copyrt xs-center">
  @lang('messages.footer.terms_reserved') 
  <a href="#">GOOD | REALTY</a>
</div>
<div class="col-sm-6 text-right xs-center d-flex justify-content-end">
  <div class="list-inline footer-social d-flex justify-content-around">
   <a href=""><i class="fab fa-facebook-square"></i></a>
   <a href=""><i class="fab fa-instagram"></i></a>
   <a href=""><i class="fab fa-youtube"></i></a>
  </div>
</div>
</div>
</div>
</div>
</section>

</footer>

</div>

     <script type="text/javascript" src="//code.jquery.com/jquery-1.12.4.js"></script>
      <!-- Bootstrap tooltips -->
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
      <!-- Bootstrap core JavaScript -->
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
      <!-- MDB core JavaScript -->
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.14.1/js/mdb.min.js"></script>
       
      <script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script> 
    </body>
</html>