@extends('layouts.app')

@section('content')
	<div class="mt-100 text-center">
		<h1 class="text-success">Հայտարարություններ</h1>	
	</div>	

	<div class="container mt-5">
		<form method="POST" action="/admin/statement" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="row justify-content-around">
				<div class="col-md-4 select-outline">
				    <select class="browser-default custom-select" name="statement_type">
				      <option class="text-dark" value="" disabled selected>Ընտրել հայտարարության տեսակը</option>
				      <option class="text-dark" value="for_rent">Վարձակալության</option>
				      <option class="text-dark" value="for_sale">Վաճառքի</option>
				    </select>
				</div>
				<div class="col-md-4 select-outline">
				    <select class="browser-default custom-select statement_part" name="statement_part">
				      <option class="text-dark" value="" disabled selected>Ընտրել հայտարարության բաժինը</option>
				      <option class="text-dark" value="own_house">Սեփական տուն</option>
				      <option class="text-dark" value="apartment">Բնակարան</option>
				      <option class="text-dark" value="commercial_area">Կոմերցիոն տարածք</option>
				      <option class="text-dark" value="land_area">Հողատարածք</option>
				    </select>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-4">
					<label for="title_statement_en" class="text-dark">
						Հայտարարության վերնագիր անգլերենով
					</label>
					<input type="text" id="title_statement_en" class="form-control" name="en_title" required>
				</div>
				<div class="col-4">
					<label for="title_statement_ru" class="text-dark">
						Հայտարարության վերնագիր ռուսերենով
					</label>
					<input type="text" id="title_statement_ru" class="form-control" name="ru_title" required>
				</div>
				<div class="col-4">
					<label for="title_statement_hy" class="text-dark">
						Հայտարարության վերնագիր հայերենով
					</label>
					<input type="text" id="title_statement_hy" class="form-control" name="hy_title" required>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-4">
					<label for="street_statement_en" class="text-dark">
						Փողոցի անունը անգլերենով
					</label>
					<input type="text" id="street_statement_en" class="form-control" name="en_street" required>
				</div>
				<div class="col-4">
					<label for="street_statement_ru" class="text-dark">
						Փողոցի անունը ռուսերենով
					</label>
					<input type="text" id="street_statement_ru" class="form-control" name="ru_street" required>
				</div>
				<div class="col-4">
					<label for="street_statement_hy" class="text-dark">
						Փողոցի անունը հայերենով
					</label>
					<input type="text" id="street_statement_hy" class="form-control" name="hy_street" required>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-4">
					<div class="md-form">
					  <textarea id="form7" class="md-textarea form-control" rows="3" name="en_text"></textarea>
					  <label for="form7">Հայտարարության համար տեքստ անգլերենով</label>
					</div>
				</div>
				<div class="col-4">
					<div class="md-form">
					  <textarea id="form7" class="md-textarea form-control" rows="3" name="ru_text"></textarea>
					  <label for="form7">Հայտարարության համար տեքստ ռուսերենով</label>
					</div>
				</div>
				<div class="col-4">
					<div class="md-form">
					  <textarea id="form7" class="md-textarea form-control" rows="3" name="hy_text"></textarea>
					  <label for="form7">Հայտարարության համար տեքստ հայերենով</label>
					</div>
				</div>
			</div>
			<div class="row mt-5 justify-content-center">
				<div class="col-2">
					<label for="price_statement" class="text-dark">
						Գինը
					</label>
					<input type="number" id="price_statement" class="form-control" name="price" required>
				</div>
				<div class="col-2 select-outline mt-auto">
				    <select class="browser-default custom-select" name="currency">
				      <option class="text-dark" value="" disabled selected>Տարադրամ</option>
				      <option class="text-dark" value="usd">USD</option>
				      <option class="text-dark" value="amd">AMD</option>
				    </select>
				</div>
				<div class="col-2">
					<label for="rooms_statement" class="text-dark">
						Սենյակների քանակ
					</label>
					<input type="number" id="rooms_statement" class="form-control room" name="rooms" required>
				</div>
				<div class="col-2">
					<label for="area_statement" class="text-dark">
						Մակերեսը
					</label>
					<input type="number" id="area_statement" class="form-control area" name="area" required>
				</div>
				<div class="col-2">
					<label for="floor_statement" class="text-dark">
						Հարկ
					</label>
					<input type="text" id="floor_statement" class="form-control floor" name="floor" required>
				</div>
				<div class="col-2 select-outline mt-auto">
				    <select class="browser-default custom-select building_type" name="building_type">
				      <option class="text-dark" value="" disabled selected>Շինության տիպը</option>
				      <option class="text-dark" value="stone">Քարե</option>
				      <option class="text-dark" value="panel">Պանելային</option>
				      <option class="text-dark" value="monolith">Մոնոլիտ</option>
				      <option class="text-dark" value="brick">Աղյուսե</option>
				      <option class="text-dark" value="tape">Կասետային</option>
				      <option class="text-dark" value="wooden">Փայտե</option>
				    </select>
				</div>
			</div>
			<div class="row mt-5 justify-content-around">
				<div class="col-md-2 select-outline">
				    <select class="browser-default custom-select" name="top_news" required>
				      <option class="text-dark" value="" disabled selected>Ընտրել հայտարարության տիպը</option>
				      <option class="text-dark" value="1">Լավագույն</option>
				      <option class="text-dark" value="0">Սովորական</option>
				    </select>
				</div>
				<div class="col-md-2 select-outline">
				    <select class="browser-default custom-select" name="region" required>
				      <option class="text-dark" value="" disabled selected>Տարածաշրջան</option>
				      <option class="text-dark" value="ajapnyak">Աջափնյակ</option>
				      <option class="text-dark" value="arabkir">Արաբկիր</option>
				      <option class="text-dark" value="avan">Ավան</option>
				      <option class="text-dark" value="davtashen">Դավիթաշեն</option>
				      <option class="text-dark" value="erebuni">Էրեբունի</option>
				      <option class="text-dark" value="qanaqer_zeytun">Քանաքեռ Զեյթուն</option>
				      <option class="text-dark" value="kentron">Կենտրոն</option>
				      <option class="text-dark" value="malatya_sebastia">Մալաթիա Սեբաստիա</option>
				      <option class="text-dark" value="nor_norq">Նոր Նորք</option>
				      <option class="text-dark" value="norq_marash">Նորք Մարաշ</option>
				      <option class="text-dark" value="nubarashen">Նուբարաշեն</option>
				      <option class="text-dark" value="shengavit">Շենգավիթ</option>
				    </select>  
				</div>
			</div>
			<div class="row mt-5 justify-content-center">	
				<div class="input-group">
				  <div class="input-group-prepend">
				    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
				  </div>
				  <div class="custom-file">
				    <input type="file" class="custom-file-input" id="inputGroupFile01"
				      aria-describedby="inputGroupFileAddon01" name="images[]" multiple>
				    <label class="custom-file-label" for="inputGroupFile01">Ընտրել նկարներ</label>
				  </div>
				</div>
			</div>
			<div class="text-right mt-5">
				<button type="submit" class="btn btn-success">Ստեղծել հայտարարություն</button>
			</div>
		</form>
	</div>
	
@endsection