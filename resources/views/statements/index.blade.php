@extends('layouts.app')

@section('content')
	<div class="mt-100 text-center">
		<h1 class="text-success">Հայտարարություններ</h1>	
	</div>
	<div class="mt-3 text-center">
		<a href="/admin/statement/create">
			<button type="button" class="btn btn-primary">Ստեղծել հայտարարություն</button>
		</a>
	</div>
	@if($statements)
	<div class="container text-center">
		<div class="row justify-content-end mt-5 mb-5">
			<div class="col-4">
				<form class="form-inline active-pink-4" method="POST" action="/admin/search-statement-index">
					@csrf
				  <input class="form-control form-control-sm mr-3 w-75" type="number" placeholder="Search" aria-label="Search" name="id">
				  <button type="submit" class="search-statement">
				  	<i class="fas fa-search" aria-hidden="true"></i>
				  </button>
				</form>
			</div>
		</div>
		<table class="table table-bordered" id="statement">
		  <thead>
		    <tr>
		      <th scope="col">Համար</th>
		      <th scope="col">Վերնագիր</th>
		      <th scope="col">Գինը</th>
		      <th></th>
		      <th></th>
		    </tr>
		  </thead>
		  <tbody>
		  	@foreach($statements as $statement)
			    <tr>
			      <td>{{ $statement->id }}</th>
			      <td>{{ $statement->hy_title }}</td>
			      <td>{{ $statement->price }}  {{ $statement->currency }}</td>
			      <td>
			      	<a href="/admin/statement/{{ $statement->id }}/edit">
			      		<button type="button" class="btn btn-info">Edit</button>
			      	</a>
			      </td>
			      <td>
			      	<button type="button" data-toggle="modal" data-target="#centralModalSuccess" class="btn btn-danger delete-statement" value="{{ $statement->id }}">
			      		Delete
			      	</button>
			      </td>
			    </tr>
		    @endforeach
		  </tbody>
		</table>
		<div class="mt-5">
			@if($paginate)
				{{ $statements->links() }}
			@endif
		</div>
	</div>
	@else
	<div class="mt-5 text-center">
		<h3 class="text-danger">Հայտարարություն չկա</h1>
	</div>
	@endif
	<div class="modal fade" id="centralModalSuccess" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	   <div class="modal-dialog modal-notify modal-danger" role="document">
	     <div class="modal-content">
	       <div class="modal-header">

	         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	           <span aria-hidden="true" class="white-text">&times;</span>
	         </button>
	       </div>
	       <div class="modal-body">
	         <div class="text-center">
	           <p>Համոզված եք , որ ուզում եք ջնջել</p>
	         </div>
	       </div>
	       <div class="modal-footer justify-content-center">
	         <button type="button" class="btn btn-danger delete" data-dismiss="modal">Այո</button>
	         <a type="button" class="btn btn-outline-success waves-effect" data-dismiss="modal">Ոչ</a>
	       </div>
	     </div>
	   </div>
 </div>
@endsection