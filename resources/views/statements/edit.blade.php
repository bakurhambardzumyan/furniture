@extends('layouts.app')

@section('content')
	<div class="mt-100 text-center">
		<h1 class="text-success">Հայտարարություններ</h1>	
	</div>	

	<div class="container mt-5">
		<form method="POST" action="/admin/statement/{{ $statement->id }}" enctype="multipart/form-data">
			<input type="hidden" name="_method" value="PUT">
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<div class="row justify-content-around">
				<div class="col-md-4 select-outline">
				    <select class="browser-default custom-select" name="statement_type">
				      <option class="text-dark" value="" disabled selected>Ընտրել հայտարարության տեսակը</option>
				      <option class="text-dark" value="for_rent" {{ $statement->statement_type == 'for_rent' ? 'selected' : '' }}>
				      	Վարձակալության
				      </option>
				      <option class="text-dark" value="for_sale" {{ $statement->statement_type == 'for_sale' ? 'selected' : '' }}>
				      	Վաճառքի
				      </option>
				    </select>
				</div>
				<div class="col-md-4 select-outline">
				    <select class="browser-default custom-select statement_part" name="statement_part">
				      <option class="text-dark" value="" disabled selected>Ընտրել հայտարարության բաժինը</option>
				      <option class="text-dark" value="own_house" {{ $statement->statement_part == 'own_house' ? 'selected' : '' }}>
				      	Սեփական տուն
				      </option>
				      <option class="text-dark" value="apartment" {{ $statement->statement_part == 'apartment' ? 'selected' : '' }}>
				      	Բնակարան
				  	  </option>
				      <option class="text-dark" value="commercial_area" {{ $statement->statement_part == 'commercial_area' ? 'selected' : '' }}>
				      	Կոմերցիոն տարածք
				      </option>
				      <option class="text-dark" value="land_area" {{ $statement->statement_part == 'land_area' ? 'selected' : '' }}>
				      	Հողատարածք
				      </option>
				    </select>
				</div>
				<div class="col-md-2 select-outline">
				    <select class="browser-default custom-select" name="top_news" required>
				      <option class="text-dark" value="" disabled selected>Ընտրել հայտարարության տիպը</option>
				      <option class="text-dark" value="1" {{ $statement->top_news == 1 ? 'selected' : '' }}>
				      	Լավագույն
				      </option>
				      <option class="text-dark" value="0" {{ $statement->top_news == 0 ? 'selected' : '' }}>
				      	Սովորական
				      </option>
				    </select>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-4">
					<label for="title_statement_en" class="text-dark">
						Հայտարարության վերնագիր անգլերենով
					</label>
					<input type="text" id="title_statement_en" class="form-control" name="en_title" required value="{{ $statement->en_title }}">
				</div>
				<div class="col-4">
					<label for="title_statement_ru" class="text-dark">
						Հայտարարության վերնագիր ռուսերենով
					</label>
					<input type="text" id="title_statement_ru" class="form-control" name="ru_title" required value="{{ $statement->ru_title }}">
				</div>
				<div class="col-4">
					<label for="title_statement_hy" class="text-dark">
						Հայտարարության վերնագիր հայերենով
					</label>
					<input type="text" id="title_statement_hy" class="form-control" name="hy_title" required value="{{ $statement->hy_title }}">
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-4">
					<label for="street_statement_en" class="text-dark">
						Փողոցի անունը անգլերենով
					</label>
					<input type="text" id="street_statement_en" class="form-control" name="en_street" required value="{{ $statement->en_street }}">
				</div>
				<div class="col-4">
					<label for="street_statement_ru" class="text-dark">
						Փողոցի անունը ռուսերենով
					</label>
					<input type="text" id="street_statement_ru" class="form-control" name="ru_street" required value="{{ $statement->ru_street }}">
				</div>
				<div class="col-4">
					<label for="street_statement_hy" class="text-dark">
						Փողոցի անունը հայերենով
					</label>
					<input type="text" id="street_statement_hy" class="form-control" name="hy_street" required value="{{ $statement->hy_street }}">
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-4">
					<div class="md-form">
					  <textarea id="form7" class="md-textarea form-control" rows="3" name="en_text">
					  	{{ $statement->en_text }}
					  </textarea>
					  <label for="form7">Հայտարարության համար տեքստ անգլերենով</label>
					</div>
				</div>
				<div class="col-4">
					<div class="md-form">
					  <textarea id="form7" class="md-textarea form-control" rows="3" name="ru_text">
					  	{{ $statement->ru_text }}
					  </textarea>
					  <label for="form7">Հայտարարության համար տեքստ ռուսերենով</label>
					</div>
				</div>
				<div class="col-4">
					<div class="md-form">
					  <textarea id="form7" class="md-textarea form-control" rows="3" name="hy_text">
					  	{{ $statement->hy_text }}
					  </textarea>
					  <label for="form7">Հայտարարության համար տեքստ հայերենով</label>
					</div>
				</div>
			</div>
			<div class="row mt-5 justify-content-center">
				<div class="col-2">
					<label for="price_statement" class="text-dark">
						Գինը
					</label>
					<input type="number" id="price_statement" class="form-control" name="price" required value="{{ $statement->price }}">
				</div>
				<div class="col-2 select-outline mt-auto">
				    <select class="browser-default custom-select" name="currency">
				      <option class="text-dark" value="" disabled selected>Տարադրամ</option>
				      <option class="text-dark" value="usd" {{ $statement->currency == "usd" ? 'selected' : '' }}> 
				      	 USD
				   	  </option>
				      <option class="text-dark" value="amd" {{ $statement->currency == "amd" ? 'selected' : '' }}>
				      	AMD
				      </option>
				    </select>
				</div>
				<div class="col-2">
					<label for="rooms_statement" class="text-dark">
						Սենյակների քանակ
					</label>
					<input type="number" id="rooms_statement" class="form-control room" name="rooms" required value="{{ $statement->rooms }}">
				</div>
				<div class="col-2">
					<label for="area_statement" class="text-dark">
						Մակերեսը
					</label>
					<input type="number" id="area_statement" class="form-control area" name="area" required value="{{ $statement->area }}">
				</div>
				<div class="col-2">
					<label for="floor_statement" class="text-dark">
						Հարկ
					</label>
					<input type="number" min="0" id="floor_statement" class="form-control floor" name="floor" required value="{{ $statement->floor }}">
				</div>
				<div class="col-2 select-outline mt-auto">
				    <select class="browser-default custom-select building_type" name="building_type">
				      <option class="text-dark" value="" disabled selected>Շինության տիպը</option>
				      <option class="text-dark" value="stone" {{ $statement->building_type == "stone" ? 'selected' : '' }}>
				      	Քարե
				      </option>
				      <option class="text-dark" value="panel" {{ $statement->building_type == "panel" ? 'selected' : '' }}>
				      	Պանելային
				      </option>
				      <option class="text-dark" value="monolith" {{ $statement->building_type == "monolith" ? 'selected' : '' }}>
				      	Մոնոլիտ
				      </option>
				      <option class="text-dark" value="brick"  {{ $statement->building_type == "brick" ? 'selected' : '' }}>
				      	Աղյուսե
				      </option>
				      <option class="text-dark" value="tape" {{ $statement->building_type == "tape" ? 'selected' : '' }}>
				      	Կասետային
				      </option>
				      <option class="text-dark" value="wooden" {{ $statement->building_type == "wooden" ? 'selected' : '' }}>
				      	Փայտե
				      </option>
				    </select>
				</div>
			</div>
			<div class="row mt-5 justify-content-center">	
				<div class="input-group">
				  <div class="input-group-prepend">
				    <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
				  </div>
				  <div class="custom-file">
				    <input type="file" class="custom-file-input" id="inputGroupFile01"
				      aria-describedby="inputGroupFileAddon01" name="images[]" multiple>
				    <label class="custom-file-label" for="inputGroupFile01">Ընտրել նոր նկարներ</label>
				  </div>
				</div>
			</div>
			<div class="row mt-5">
				@foreach($statement->images as $image)
					<div class="position-relative col-3 mr-3"> 
						<button type="button" class="delete-img" value="{{ $image->id }}" data-statement="{{ $statement->id }}"><i class="far fa-times-circle"></i></button>
						<img src="{{'/images/statements/' . $statement->id .'/'. $image->img_name }}" class="w-100 h-100">
					</div>
				@endforeach
			</div>
			<div class="text-right mt-5">
				<button type="submit" class="btn btn-success">Թարացնել հայտարարություն</button>
			</div>
		</form>
	</div>
	
@endsection