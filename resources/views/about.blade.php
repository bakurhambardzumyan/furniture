@extends('layouts.app')


@section('content')

<div class="about-bg">
  <h2 class="mt-5">@lang('messages.menu.about_us')</h2>
</div>

<div class="about container">
<div class="ABOUT">
  <div class="about-us">
    <h2>@lang('messages.about_us.by_or_sell')</h2>
    <p class="mt-4">
      @lang('messages.about_us.by_or_sell_text')
    </p>
    <p id="box" class="boxreadmore">@lang('messages.about_us.learn_more_text')</p>

    <button class="learntopnewsbutt mt-5" id="aboutbuttn">@lang('messages.agency.learn_more')</button>
  </div>

  <div class="different mt-5">
    <h2>@lang('messages.about_us.different_why')</h2>
    <p class="mt-4">@lang('messages.about_us.different_why_text')</p>
  </div>
</div>

<div class="about-img">
  <img src="../images/img2.png" alt="">
  
  <div class="information">
    <p><i class="fas fa-check"> </i>@lang('messages.about_us.text_1')</p>
    <p><i class="fas fa-check"> </i>@lang('messages.about_us.text_2')</p>
    <p><i class="fas fa-check"> </i>@lang('messages.about_us.text_3')</p>
    <p><i class="fas fa-check"> </i>@lang('messages.about_us.text_4')</p>
    <p><i class="fas fa-check"> </i>@lang('messages.about_us.text_5')</p>
    <p><i class="fas fa-check"> </i>@lang('messages.about_us.text_6')</p>
  </div>
</div>  



</div>
<div class="text-center btnmap">

  <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalRegular">Regular map modal</button>

</div>

<!--Modal: Name-->
<div class="modal fade" id="modalRegular" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">

    <!--Content-->
    <div class="modal-content">

      <!--Body-->
      <div class="modal-body mb-0 p-0">

        <!--Google map-->
        <div id="map-container-google-16" class="z-depth-1-half map-container-9" style="height: 400px">
          <iframe src="https://maps.google.com/maps?q=new%20delphi&t=&z=13&ie=UTF8&iwloc=&output=embed"
            frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>

      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">

        <button type="button" class="btn btn-outline-success btn-md" data-dismiss="modal">Close <i class="fas fa-times ml-1"></i></button>

      </div>

    </div>
    <!--/.Content-->

  </div>
</div>

@endsection