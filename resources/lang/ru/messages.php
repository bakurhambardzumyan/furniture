<?php

return [
    'welcome' => 'Добро пожаловать в наше приложение',

    'show_statement' => 
    	[
			'building_type' => 'Тип здания',
    		'room' => 'Комнаты',
    		'space' => 'Площадь',
    		'floor' => 'Этаж',
    		'information' => 'Информация',
    		'price' => 'Цена'
    	],

    'menu' => 
    	[
    		'home' => 'Главная',
    		'about_us' => 'О нас',
    		'services' => 'Сервисы',
    		'contact_us' => 'Контакт',
    		'for_sale' => 'Продажа',
    		'for_rent' => 'В аренду'
    	],

    'region' => 
    	[
    		'ajapnyak' => 'Ачапняк',
    		'arabkir' => 'Арабкир',
    		'avan' => 'Аван',
    		'davtashen' => 'Давидашен',
    		'erebuni' => 'Эребуни',
    		'qanaqer_zeytun' => 'Зейтун Канакер',
    		'kentron' => 'Кентрон',
    		'malatya_sebastia' => 'Малатия Себастия',
    		'nor_norq' => 'Нор Норк',
    		'norq_marash' => 'Нор Мараш',
    		'nubarashen' => 'Нубарашен',
    		'shengavit' => 'Шенгавит'
    	],

    'building_types' => 
    	[
    		'stone' => 'Каменное',
    		'panel' => 'Панельное',
    		'monolith' => 'Монолит',
    		'brick' => 'Кирпичное',
    		'tape' => 'Кассетное',
    		'wooden' => 'Деревянное',
    	],	

	'statement_part' => 
    	[
    		'own_house' => 'Собственный дом',
    		'apartment' => 'Kвартира',
    		'commercial_area' => 'Коммерческая площадь',
    		'land_area' => 'Площадь земельного участка'
    	],

    'contact_us' => 
        [
            'get_in_touch' => 'Связаться с нами',
            'name' => 'Ваше имя',
            'email' => 'Введите адрес электронной почты',
            'phone' => 'Ваш телефон',
            'message' => 'Ваше сообщение',
            'ad_reference' => 'Рекламная ссылка',
            'send' => 'Отправлять',
            'follow_us' => 'Подписывайтесь на нас',
            'mail_sended' => 'Почта отправлено',
            'success_message_mail' => 'Спасибо за сообщение!',
            'our_data' => 'Наши Данные',
        ],

    'agency' => 
        [
            'slide_text_1' => 'ВСЕ ХОРОШОЕ ВОЗМОЖНО С НАМИ',
            'slide_context_1' => 'Наша компания имеет базу данных, которая ежедневно обновляется и заменяется новой информацией, обеспечивая качественное обслуживание',
            'slide_text_2' => 'Откройте для себя дом своей мечты',
            'slide_context_2' => 'Поиск тысячи продаж и аренды квартир, домов, ферм,  с профессиональными фотографиями',
            'slide_text_3' => 'Опытные Агенты',
            'slide_context_3' => 'Знать, кому доверять, может быть трудным решением для многих: Мы уверены в мастерстве наших агентов',
            'top' => 'Главные',
            'news' => 'Объявление',
            'view_property' => 'Посмотреть Детали',
            'learn_more' => 'Узнать Больше',
            'services_text' => 'Мы сделаем все возможное, чтобы удовлетворить наших клиентов и сделать правильный выбор недвижимости',
            'home' => 'Дома',
            'home_text' => 'С нашей помощью вы найдете дом своей мечты даже с возможностью ипотеки',
            'apartment' => 'Квартиры',
            'apartment_text' => 'Устали от мучительных постоянных переездов? Мы поможем вам найти квартиру, которую вы ищете',
            'land' => 'Земельные Участки',
            'land_text' => 'Постройте вместе с нами свой дом или бизнес-центр так, как вы хотите',
            'last' => 'Недавние',  
            'why_choose_use' => 'Почему выбирают нас?',
            'why_choose_use_text' => 'ВСЕ ХОРОШОЕ ВОЗМОЖНО С НАМИ',
            'mortgage_loan' => 'Ипотечный кредит',
            'mortgage_loan_text' => 'Наши специалисты помогут вам выбрать наиболее подходящую кредитную программу',
            'experienced_professionals' => 'Опытные специалисты',
            'experienced_professionals_text' => 'Специалисты компании имеют более 8 лет опыта работы на рынке недвижимости',
            'diversity' => 'Разнообразие',
            'diversity_text' => 'Наша компания имеет базу данных, которая ежедневно обновляется и заменяется новой информацией',
            'work_experience' => 'Опыт работы',
            'work_experience_text' => 'У нас отличная репутация среди партнеров и клиентов, и мы гордимся нашим профессионализмом',
            'individual_approach' => 'Индивидуальный подход',
            'individual_approach_text' => 'Наши специалисты обеспечивают индивидуальный подход к каждому клиенту',
            'design' => 'ДИЗАЙН',
            'design_text' => 'Агентство недвижимости Good Realty предлагает инновационные решения для вашей квартиры.',
        ],   

    'footer' => 
        [
            'opening_hours' => 'Рабочие дни',
            'monday_sunday' => 'Понедельник - Воскресенье',
            'support' => 'Cвязь',
            'text_good_reality' => 'Агентство недвижимости Good-Realty было основано в 2015 году.  Высокое квалификация наших специалистов гарантирует немедленное решение любых проблем наших клиентов.',
            'contact_information' => 'Контактные информации',
            'read_more' => 'Читать Подробнее',
            'terms_reserved' => '© 2015-2020 ВСЕ ПРАВА ЗАЩИЩЕНЫ',
            'top' => 'Верх',
        ],    

    'about_us' => 
        [
            'by_or_sell' => 'Купите или продайте свой дом за несколько секунд',
            'by_or_sell_text' => 'Агентство недвижимости Good-Realty было основано в 2015 году и является одним из ведущих агентств в Армении;  Специалисты компании имеют более 8 лет опыта работы на рынке недвижимости.  Высокое качество наших специалистов гарантирует немедленное решение любой сложности наших клиентов: «www ........ am» - это специализированный сайт по недвижимости.  Предоставляет исчерпывающую и исчерпывающую информацию о местном рынке недвижимости.',
            'learn_more_text' => 'Сайт оснащен необходимыми и точными данными которые регулярно обновляются.  На сайте подробно описаны объекты недвижимости, информация о строительстве, а также окрестности.',
            'different_why' => 'Почему мы отличаемся от других', 
            'different_why_text' => 'Мы предоставляем качественный сервис, полную информацию, быстрое реагирование на жалобы, своевременное выполнение обязательств.Клиентам предоставляется возможность делать правильный и качественный выбор при покупке, продаже и аренде недвижимости с бесплатными консультациями опытных и профессиональных сотрудников.', 
            'text_1' => 'Мы предоставляем лучшие инновационные решения',
            'text_2' => 'Мы поддерживаем наших клиентов на расстоянии',
            'text_3' => 'База данных, которая обновляется ежедневно',
            'text_4' => 'Партнерские отношения с местными агентствами',
            'text_5' => 'Мы готовы работать со всеми',
            'text_6' => 'Мы предоставляем качественный сервис',
        ],        	
];

