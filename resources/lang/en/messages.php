<?php

return [
    'welcome' => 'Welcome to our application',

    'show_statement' => 
    	[
    		'building_type' => 'Building Type',
    		'room' => 'Rooms',
    		'space' => 'Space',
    		'floor' => 'Floor',
    		'information' => 'Information',
    		'price' => 'Price'
    	],
    
    'menu' => 
    	[
    		'home' => 'Home',
    		'about_us' => 'About Us',
    		'services' => 'Services',
    		'contact_us' => 'Contact Us',
    		'for_sale' => 'For Sale',
    		'for_rent' => 'For Rent'
    	],

    'region' => 
    	[
    		'ajapnyak' => 'Ajapnyak',
    		'arabkir' => 'Arabkir',
    		'avan' => 'Avan',
    		'davtashen' => 'Davitashen',
    		'erebuni' => 'Erebuni',
    		'qanaqer_zeytun' => 'Kanaker-Zeytun',
    		'kentron' => 'Kentron',
    		'malatya_sebastia' => 'Malatia-Sebastia',
    		'nor_norq' => 'Nor Nork',
    		'norq_marash' => 'Nork-Marash',
    		'nubarashen' => 'Nubarashen',
    		'shengavit' => 'Shengavit'
    	],

    'building_types' => 
    	[
    		'stone' => 'Stone',
    		'panel' => 'Panels',
    		'monolith' => 'Monolith',
    		'brick' => 'Bricks',
    		'tape' => 'Cassette',
    		'wooden' => 'Wooden',
    	],

	'statement_part' => 
    	[
    		'own_house' => 'Own House',
    		'apartment' => 'Apartment',
    		'commercial_area' => 'Commercial area',
    		'land_area' => 'Land area'
    	],

    'contact_us' => 
        [
            'get_in_touch' => 'To contact us',
            'name' => 'Your Name',
            'email' => 'Type your email',
            'phone' => 'Your Phone',
            'message' => 'Your message',
            'ad_reference' => 'Ad reference',
            'send' => 'Send',
            'follow_us' => 'Follow Us',
            'mail_sended' => 'Mail Sended',
            'success_message_mail' => 'Thanks, for the message!',
            'our_data' => 'Our Data',
        ],

    'agency' => 
        [
            'slide_text_1' => 'ALL GOOD IS POSSIBLE WITH US',
            'slide_context_1' => 'Our company has a database that is updated daily and replaced with new information, providing quality services',
            'slide_text_2' => 'Discover your dream home',
            'slide_context_2' => 'Search thousands of sales and rental apartments, houses, farms, with professional photos ․',
            'slide_text_3' => 'Experienced Agents',
            'slide_context_3' => 'Knowing which agent to trust can be a difficult decision for many. We are confident in the skill of our agents',
            'top' => 'Top',
            'news' => 'Ads',
            'view_property' => 'View Detalis',
            'learn_more' => 'Learn More',
            'services_text' => 'We will do our best to satisfy our customers and make the right choice of real estate',
            'home' => 'Houses',
            'home_text' => 'With our help you will find your dream home even with mortgage option',
            'apartment' => 'Apartaments',
            'apartment_text' => 'Tired of painful constant movings?Wee will help you to find the apartment you are looking for',
            'land' => 'Land',
            'land_text' => 'Build your home or business center with us the way you want',
            'last' => 'Last',  
            'why_choose_use' => 'Why choose us?',
            'why_choose_use_text' => 'All good is possible with us',
            'mortgage_loan' => 'Mortgage loan',
            'mortgage_loan_text' => 'Our experts will help you choose the most suitable loan program',
            'experienced_professionals' => 'Experienced professionals',
            'experienced_professionals_text' => 'The companies specialists have more than 8 years of experience in the real estate market',
            'diversity' => 'Diversity',
            'diversity_text' => 'Our company has a database that is updated daily and replaced with new information',
            'work_experience' => 'Work experience',
            'work_experience_text' => 'We have an excellent reputation among partners and customers, and we are proud of our professionalism',
            'individual_approach' => 'Individual approach',
            'individual_approach_text' => 'Our experts provide an individual approach to each client',
            'design' => 'DESIGN',
            'design_text' => 'Good Realty Real Estate Agency offers innovative solutions for your apartment.',
        ],  

    'footer' => 
        [
            'opening_hours' => 'Working days',
            'monday_sunday' => 'Monday - Sunday',
            'support' => 'Support',
            'text_good_reality' => 'Good-Realty Real Estate Agency was founded in 2015.  High qualification of our specialists guarantees an immediate solution to any problems of our customers.',
            'contact_information' => 'Contact Information',
            'read_more' => 'Read More',
            'terms_reserved' => '© 2015-2020 ALL RIGHTS RESERVED. GOOD REALTY',
            'top' => 'Top',
        ],

    'about_us' => 
        [
            'by_or_sell' => 'Buy or sell your house in few seconds',
            'by_or_sell_text' => 'Good-Realty Real Estate Agency was founded in 2017 and is one of the leading agencies in Armenia;  The companies specialists have more than 8 years of experience in the real estate market.  The high quality of our specialists guarantees an immediate solution to any complexity of our clients: "www ........ am" is a specialized real estate site.  Provides comprehensive and comprehensive information about the local real estate market.',
            'learn_more_text' => 'The site is equipped with the necessary and accurate data, which are regularly updated.  The site describes in detail real estate, information about the construction, as well as the surrounding area.',
            'different_why' => 'Why are we different from the others', 
            'different_why_text' => 'We provide high-quality service, complete information, quick response to complaints, timely fulfillment of obligations. Customers are given the opportunity to make the right and high-quality choice when buying, selling and renting real estate with free advice from experienced and professional employees.', 
            'text_1' => 'We provide best innovation solutions',
            'text_2' => 'We support our clients in distance',
            'text_3' => 'The database is updated daily',
            'text_4' => 'Strong partnerships with local agencies',
            'text_5' => 'We are ready to work with everyone',
            'text_6' => 'We provide quality service',
        ],
];