<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('statement_type');
            $table->string('statement_part');
            $table->string('en_title');
            $table->string('ru_title');
            $table->string('hy_title');
            $table->string('en_street');
            $table->string('ru_street');
            $table->string('hy_street');
            $table->longText('en_text');
            $table->longText('ru_text');
            $table->longText('hy_text');
            $table->integer('price');
            $table->string('currency');
            $table->integer('rooms')->nullable();
            $table->integer('area');
            $table->integer('floor')->nullable();
            $table->integer('top_news')->nullable();
            $table->string('building_type')->nullable();
            $table->string('region');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statements');
    }
}