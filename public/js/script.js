$( document ).ready(function() {
	
    let sendMail = $('.send-success');
    if(sendMail.length > 0) {
    	 $('.mail-success').modal('show');
    }
$(function () {
  $('.material-tooltip-main').tooltip({
    template: '<div class="tooltip md-tooltip"><div class="tooltip-arrow md-arrow"></div><div class="tooltip-inner md-inner"></div></div>'
  });
})
$('#aboutbuttn').click(function() {
    $(this).siblings(".boxreadmore").toggle();
  });
$('.dropdown').mouseenter(function() {
        $('.dropdown-menu').fadeIn();
    });

    $('.dropdownli').mouseleave(function(){
        $('.dropdown-menu').fadeOut();
    });
$(function () {
   $(document).scroll(function () {
    var $nav = $(".navbar-fixed-top");
    $nav.toggleClass('scrolled', $(this).scrollTop() >= 100);
  });
});
$(function () {
   $(document).scroll(function () {
    var $navbefore = $(".navbar-brandbef");
    $navbefore.toggleClass('scrollicon', $(this).scrollTop() >= 100);
  });
});

// handles the carousel thumbnails
// https://stackoverflow.com/questions/25752187/bootstrap-carousel-with-thumbnails-multiple-carousel
$('[id^=carousel-selector-]').click(function() {
  var id_selector = $(this).attr('id');
  var id = parseInt( id_selector.substr(id_selector.lastIndexOf('-') + 1) );
  $('#myCarousel').carousel(id);
});
// Only display 3 items in nav on mobile.
if ($(window).width() < 575) {
  $('#carousel-thumbs .row div:nth-child(4)').each(function() {
    var rowBoundary = $(this);
    $('<div class="row mx-0">').insertAfter(rowBoundary.parent()).append(rowBoundary.nextAll().addBack());
  });
  $('#carousel-thumbs .carousel-item .row:nth-child(even)').each(function() {
    var boundary = $(this);
    $('<div class="carousel-item">').insertAfter(boundary.parent()).append(boundary.nextAll().addBack());
  });
}
// Hide slide arrows if too few items.
if ($('#carousel-thumbs .carousel-item').length < 2) {
  $('#carousel-thumbs [class^=carousel-control-]').remove();
  $('.machine-carousel-container #carousel-thumbs').css('padding','0 5px');
}
// when the carousel slides, auto update
$('#myCarousel').on('slide.bs.carousel', function(e) {
  var id = parseInt( $(e.relatedTarget).attr('data-slide-number') );
  $('[id^=carousel-selector-]').removeClass('selected');
  $('[id=carousel-selector-'+id+']').addClass('selected');
});
// when user swipes, go next or previous
$('#myCarousel').swipe({
  fallbackToMouseEvents: true,
  swipeLeft: function(e) {
    $('#myCarousel').carousel('next');
  },
  swipeRight: function(e) {
    $('#myCarousel').carousel('prev');
  },
  allowPageScroll: 'vertical',
  preventDefaultEvents: false,
  threshold: 75
});
/*
$(document).on('click', '[data-toggle="lightbox"]', function(event) {
  event.preventDefault();
  $(this).ekkoLightbox();
});
*/

$('#myCarousel .carousel-item img').on('click', function(e) {
  var src = $(e.target).attr('data-remote');
  if (src) $(this).ekkoLightbox();
});



});
var mybutton = document.getElementById("myBtn");
window.onscroll = function() {scrollFunction()};
function scrollFunction() {
  if (document.body.scrollTop >= 600 || document.documentElement.scrollTop >= 600) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}
function topFunction() {
  document.body.scrollTop = 1;
  document.documentElement.scrollTop = 1;
}

function phoneKey(key) {
  return (key >= '0' && key <= '9') || key == '+' ||
  key == '(' || key == ')' || key == '-' ||
    key == 'ArrowLeft' || key == 'ArrowRight'
    || key == 'Delete' || key == 'Backspace';
}
// new WOW().init();

$(document).ready(function() {
  $('.mdb-select').materialSelect();
});

$('.statement_part')


$('.statement_part').change(function() {
    var val = $(".statement_part option:selected").val();
    if(val == "commercial_area" || val == "land_area") {
      $('.building_type').prop('disabled', true);
      $('.room').prop('disabled', true);
    } else {
      $('.building_type').prop('disabled', false);
      $('.room').prop('disabled', false);
    }

    if(val == "land_area") {
      $('.floor').prop('disabled', true);
    } else {
      $('.floor').prop('disabled', false);
    }
});

var id = 0;
var parent = '';

$('.delete-statement').click(function() {
  id = $(this).val();
  parent = $(this).parent().parent();
})

$('.delete').click(function() {
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    url: "/admin/statement/"+id,
    type: "DELETE",
    data: {
      id : id
    },
    success: function(res){
      if(res == 1) {
        parent.hide();
      }
    }
  });
})

$('.delete-img').click(function() {
  var statement_id = $(this).data('statement');
  var img_id = $(this).val();
  var parent = $(this).parent();
  $.ajax({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    },
    url: "/admin/delete-img",
    type: "POST",
    data: {
      img_id : img_id,
      statement_id: statement_id
    },
    success: function(res){
        parent.hide();
    }
  });
})
